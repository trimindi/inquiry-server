package com.trimindi.pln.inquiryServer.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Created by sx on 14/05/17.
 */
@Provider
public class CORSFilter implements ContainerResponseFilter {
    public static final Logger logger = LoggerFactory.getLogger(CORSFilter.class);
    @Context
    private HttpServletRequest sr;
    /*
     * (non-Javadoc)
     *
     * @see javax.ws.rs.container.ContainerResponseFilter#filter(javax.ws.rs.
     * container.ContainerRequestContext,
     * javax.ws.rs.container.ContainerResponseContext)
     */
    @Override
    public void filter(ContainerRequestContext containerRequestContext, ContainerResponseContext containerResponseContext) throws IOException {
        containerResponseContext.getHeaders().add("Access-Control-Allow-Origin", "*");
        containerResponseContext.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
        containerResponseContext.getHeaders().add("Access-Control-Allow-Credentials", "true");
        containerResponseContext.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
        containerResponseContext.getHeaders().add("Access-Control-Max-Age", "1209600");
//        logger.error("Outgoing Response to " + sr.getRemoteAddr() + " " + containerResponseContext.getEntity());
    }


}
