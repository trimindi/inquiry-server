package com.trimindi.pln.inquiryServer.filter;


import com.trimindi.pln.inquiryServer.models.PartnerCredential;
import com.trimindi.pln.inquiryServer.repository.PartnerCredentialRepository;
import com.trimindi.pln.inquiryServer.utils.constanta.Constanta;
import com.trimindi.pln.inquiryServer.utils.constanta.ResponseCode;
import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

/**
 * Created by sx on 13/05/17.
 */


@Provider
public class SecureEndpoint implements ContainerRequestFilter {
    private static final Logger logger = LoggerFactory.getLogger(SecureEndpoint.class);
    @Autowired
    PartnerCredentialRepository partnerCredentialRepository;
    @Context
    private HttpServletRequest sr;
    private String pattern;


    @Override
    public void filter(ContainerRequestContext req) throws IOException {
        Response SERVER_UNAUTORIZED = Response.status(200).entity(ResponseCode.UNAUTHORIZED_REQUEST).build();
        Response SERVER_IP_BLOCK = Response.status(200).entity(ResponseCode.UNREGISTERD_IP_ADDRESS).build();

        String userid = req.getHeaderString("userid");
        String sign = req.getHeaderString("sign");
        String time = req.getHeaderString("time");
        pattern = "Inqcome Request from " + sr.getRemoteAddr() + "-> ";
        if(userid == null || sign == null || time == null){
            req.abortWith(SERVER_UNAUTORIZED);
            logger.error(pattern + "ERROR - Userid or sign or time null");
            logger.error(pattern + "Request Method " + req.getMethod());
            logger.error(pattern + "Request Header " + req.getHeaders().toString());
            return;
        }
        if(userid.isEmpty() || sign.isEmpty() || time.isEmpty()){
            req.abortWith(SERVER_UNAUTORIZED);
            logger.error(pattern + "ERROR - Userid or sign or time empty");
            logger.error(pattern + "Request Method " + req.getMethod());
            logger.error(pattern + "Request Header " + req.getHeaders().toString());
            return;
        }
        PartnerCredential partnerCredential = partnerCredentialRepository.findOne(userid);
        try{
            if(partnerCredential != null){
                if(partnerCredential.getIp_address().equalsIgnoreCase("*") || partnerCredential.getIp_address().equalsIgnoreCase(sr.getRemoteAddr())){
                    String pass = DigestUtils.sha1Hex(partnerCredential.getPartner_uid() + time + partnerCredential.getPartner_password() + partnerCredential.getMerchant_type());
                    if(sign.equals(pass)){
                        req.setProperty(Constanta.MAC , req.getHeaderString(Constanta.MAC));
                        req.setProperty(Constanta.IP_ADDRESS , sr.getRemoteAddr());
                        req.setProperty(Constanta.PRINCIPAL , partnerCredential);
                        req.setProperty(Constanta.LATITUDE,req.getHeaderString(Constanta.LATITUDE));
                        req.setProperty(Constanta.LONGITUDE,req.getHeaderString(Constanta.LONGITUDE));
                    }else {
                        logger.error(pattern + "ERROR - sign key not equal");
                        logger.error(pattern + "Request Method " + req.getMethod());
                        logger.error(pattern + "Request Header " + req.getHeaders().toString());
                        req.abortWith(SERVER_UNAUTORIZED);
                    }
                }else{
                    logger.error(pattern + "ERROR - ip blocked");
                    logger.error(pattern + "Request Method " + req.getMethod());
                    logger.error(pattern + "Request Header " + req.getHeaders().toString());
                    req.abortWith(SERVER_IP_BLOCK);
                }
            }else{
                logger.error(pattern + "ERROR - userid not found in database");
                logger.error(pattern + "Request Method " + req.getMethod());
                logger.error(pattern + "Request Header " + req.getHeaders().toString());
                req.abortWith(SERVER_UNAUTORIZED);
            }
        }catch (Exception e){
            logger.error(pattern + "ERROR - Terjadi kesalahan " + e.getLocalizedMessage());
            logger.error(pattern + "Request Method " + req.getMethod());
            logger.error(pattern + "Request Header " + req.getHeaders().toString());
        }
    }
}