package com.trimindi.pln.inquiryServer.utils.constanta;

/**
 * Created by sx on 13/05/17.
 */
public class MachineType {
    public static final String BANK_TELLER = "6010";
    public static final String BANK_ATM = "6011";
    public static final String BANK_POS = "6012";
    public static final String BANK_DEBIT = "6013";
    public static final String BANK_INTERNET = "6014";
    public static final String BANK_KiosK = "6015";
    public static final String BANK_PHONE_BANKING = "6016";
    public static final String BANK_MOBILE_BANKING = "6017";
    public static final String BANK_EDC = "6018";
    public static final String BANK_ADM = "6019";
    public static final String PC = "6021";
    public static final String EDC = "6022";
    public static final String SMS = "6023";
    public static final String MODERN_CHANNEL = "6024";
    public static final String MOBILE_POS = "6025";
}
