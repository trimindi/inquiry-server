package com.trimindi.pln.inquiryServer.utils.iso.models;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by HP on 15/05/2017.
 */
public class IsoField {
    private String LR;
    private String PADDWITH;
    private int size;
    private String value;
    private int index;

    public IsoField(String value, int size, String PADDWITH, String LR) {
        this.LR = LR;
        this.size = size;
        this.PADDWITH = PADDWITH;
        this.value = value;
    }

    public IsoField(String value,int field){
        this.value = value;
        this.index = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        if(LR == "R"){
            return StringUtils.rightPad(value,size,PADDWITH);
        }else{
            return StringUtils.leftPad(value,size,PADDWITH);
        }
    }

}
