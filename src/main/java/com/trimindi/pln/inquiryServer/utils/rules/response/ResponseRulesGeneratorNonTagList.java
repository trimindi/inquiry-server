package com.trimindi.pln.inquiryServer.utils.rules.response;



import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class ResponseRulesGeneratorNonTagList {
    public static List<Rules> nonTagListInquiryResponse(int bitnumber, boolean success) {
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Registration Number", 32));
                R.add(new Rules<Integer>(3, "Area Code", 2));
                R.add(new Rules<Integer>(4, "Transaksi Code", 3));
                R.add(new Rules<String>(5, "Transaksi Name", 25));

                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(6, "Registration Date", 8));
                    R.add(new Rules<Integer>(7, "Expiration Date", 8));
                    R.add(new Rules<Integer>(8, "Subsriber ID", 12));
                    R.add(new Rules<String>(9, "Subsriber Name", 25));
                    R.add(new Rules<String>(10, "PLN Reference Number", 32));
                    R.add(new Rules<String>(11, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(12, "Service Unit ", 5));
                    R.add(new Rules<String>(13, "Service Unit Address", 35));
                    R.add(new Rules<String>(14, "Service Unit Phone", 15));
                    R.add(new Rules<Integer>(15, "Total Transaksi Amount Minor Unit", 1));
                    R.add(new Rules<Integer>(16, "Total Transaksi Amount", 17));
                    R.add(new Rules<Integer>(17, "PLN-BILL Minor Unit", 1));
                    R.add(new Rules<Integer>(18, "PLN-BILL IsoField (RPTAG)", 17));
                    R.add(new Rules<String>(19, "Administration Charge Minor Unit", 1));
                    R.add(new Rules<String>(20, "Administration Charge (AC)", 10));
                }

                break;
            case 62:
                R.clear();
                R.add(new Rules<Integer>(1, "Bill component type", 2));
                if(success){
                    R.add(new Rules<Integer>(2, "Bill Component Minor Unit", 1));
                    R.add(new Rules<Integer>(3, "Bill Component IsoField Amount", 17));
                }
                break;
        }
        return R;
    }

    public static List<Rules> nonTagListPaymentResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Registration Number", 32));
                R.add(new Rules<Integer>(3, "Area Code", 2));
                R.add(new Rules<Integer>(4, "Transaksi Code", 3));
                R.add(new Rules<String>(5, "Transaksi Name", 25));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(6, "Registration Date", 8));
                    R.add(new Rules<Integer>(7, "Expiration Date", 8));
                    R.add(new Rules<Integer>(8, "Subsriber ID", 12));
                    R.add(new Rules<String>(9, "Subsriber Name", 25));
                    R.add(new Rules<String>(10, "PLN Reference Number", 32));
                    R.add(new Rules<String>(11, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(12, "Service Unit ", 5));
                    R.add(new Rules<String>(13, "Service Unit Address", 35));
                    R.add(new Rules<String>(14, "Service Unit Phone", 15));
                    R.add(new Rules<Integer>(15, "Total Transaksi Amount Minor Unit", 1));
                    R.add(new Rules<Integer>(16, "Total Transaksi Amount", 17));
                    R.add(new Rules<Integer>(17, "PLN-BILL Minor Unit", 1));
                    R.add(new Rules<Integer>(18, "PLN-BILL IsoField (RPTAG)", 17));
                    R.add(new Rules<String>(19, "Administration Charge Minor Unit", 1));
                    R.add(new Rules<String>(20, "Administration Charge (AC)", 10));
                }
                break;
            case 61:
                R.clear();
                R.add(new Rules<Integer>(1, "Mutation Number", 32));
                R.add(new Rules<String>(2, "Subscriber Segmentation", 4));
                R.add(new Rules<Integer>(3, "Power Consuming Category", 9));
                if (success){
                    R.add(new Rules<String>(4, "PT. Bank Bukopin, Tbk Reference", 32));
                }
                break;
            case 62:
                R.clear();
                R.add(new Rules<Integer>(1, "Bill component type", 2));
                R.add(new Rules<Integer>(2, "Bill Component Minor Unit", 1));
                if(success){
                    R.add(new Rules<Integer>(3, "Bill Component IsoField Amount", 17));
                }
                break;
        }
        return R;
    }

    public static List<Rules> nonTagListReversalResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Registration Number", 32));
                R.add(new Rules<Integer>(3, "Area Code", 2));
                R.add(new Rules<Integer>(4, "Transaksi Code", 3));
                R.add(new Rules<String>(5, "Transaksi Name", 25));

                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(6, "Registration Date", 8));
                    R.add(new Rules<Integer>(7, "Expiration Date", 8));
                    R.add(new Rules<Integer>(8, "Subsriber ID", 12));
                    R.add(new Rules<String>(9, "Subsriber Name", 25));
                    R.add(new Rules<String>(10, "PLN Reference Number", 32));
                    R.add(new Rules<String>(11, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(12, "Service Unit ", 5));
                    R.add(new Rules<String>(13, "Service Unit Address", 35));
                    R.add(new Rules<String>(14, "Service Unit Phone", 15));
                    R.add(new Rules<Integer>(15, "Total Transaksi Amount Minor Unit", 1));
                    R.add(new Rules<Integer>(16, "Total Transaksi Amount", 17));
                    R.add(new Rules<Integer>(17, "PLN-BILL Minor Unit", 1));
                    R.add(new Rules<Integer>(18, "PLN-BILL IsoField (RPTAG)", 17));
                    R.add(new Rules<String>(19, "Administration Charge Minor Unit", 1));
                    R.add(new Rules<String>(20, "Administration Charge (AC)", 10));
                }
                break;
            case 56:
                R.add(new Rules<Integer>(1, "Original MTI", 4));
                R.add(new Rules<Integer>(2, "Original System Trace Audit Number (STAN)", 12));
                R.add(new Rules<Integer>(3, "Original Date & Time Local Transaksi", 14));
                R.add(new Rules<Integer>(4, "Original Bank Code", 7));
                break;
            case 61:
                R.clear();
                R.add(new Rules<Integer>(1, "Mutation Number", 32));
                R.add(new Rules<String>(2, "Subscriber Segmentation", 4));
                R.add(new Rules<Integer>(3, "Power Consuming Category", 9));
                break;
            case 62:
                R.clear();
                R.add(new Rules<Integer>(1, "Bill component type", 2));
                if(success){
                    R.add(new Rules<Integer>(2, "Bill Component Minor Unit", 1));
                    R.add(new Rules<Integer>(3, "Bill Component IsoField Amount", 17));
                }
                break;
        }
        return R;
    }

    public static List<Rules> nonTagListReversalRepeatResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Registration Number", 32));
                R.add(new Rules<Integer>(3, "Area Code", 2));
                R.add(new Rules<Integer>(4, "Transaksi Code", 3));
                R.add(new Rules<String>(5, "Transaksi Name", 25));

                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(6, "Registration Date", 8));
                    R.add(new Rules<Integer>(7, "Expiration Date", 8));
                    R.add(new Rules<Integer>(8, "Subsriber ID", 12));
                    R.add(new Rules<String>(9, "Subsriber Name", 25));
                    R.add(new Rules<String>(10, "PLN Reference Number", 32));
                    R.add(new Rules<String>(11, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(12, "Service Unit ", 5));
                    R.add(new Rules<String>(13, "Service Unit Address", 35));
                    R.add(new Rules<String>(14, "Service Unit Phone", 15));
                    R.add(new Rules<Integer>(15, "Total Transaksi Amount Minor Unit", 1));
                    R.add(new Rules<Integer>(16, "Total Transaksi Amount", 17));
                    R.add(new Rules<Integer>(17, "PLN-BILL Minor Unit", 1));
                    R.add(new Rules<Integer>(18, "PLN-BILL IsoField (RPTAG)", 17));
                    R.add(new Rules<String>(19, "Administration Charge Minor Unit", 1));
                    R.add(new Rules<String>(20, "Administration Charge (AC)", 10));
                }
                break;
            case 56:
                R.add(new Rules<Integer>(1, "Original MTI", 4));
                R.add(new Rules<Integer>(2, "Original System Trace Audit Number (STAN)", 12));
                R.add(new Rules<Integer>(3, "Original Date & Time Local Transaksi", 14));
                R.add(new Rules<Integer>(4, "Original Bank Code", 7));
                break;
            case 61:
                R.clear();
                R.add(new Rules<Integer>(1, "Mutation Number", 32));
                R.add(new Rules<String>(2, "Subscriber Segmentation", 4));
                R.add(new Rules<Integer>(3, "Power Consuming Category", 9));
                break;
            case 62:
                R.clear();
                R.add(new Rules<Integer>(1, "Bill component type", 2));
                if(success){
                    R.add(new Rules<Integer>(2, "Bill Component Minor Unit", 1));
                    R.add(new Rules<Integer>(3, "Bill Component IsoField Amount", 17));
                }
                break;
        }
        return R;
    }


}
