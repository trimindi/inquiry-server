package com.trimindi.pln.inquiryServer.utils.iso;



import com.trimindi.pln.inquiryServer.utils.iso.models.IsoField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class FieldBuilder {
    public static class Builder{
        private List<IsoField> list;
        private StringBuilder results;
        public Builder(){
            list = new ArrayList<>();
        }
        public Builder addValue(IsoField v){
            this.list.add(v);
            return this;
        }
        public Builder addValue(String value, int size, String PADDWITH, String LR){
            this.list.add(new IsoField(value,size,PADDWITH,LR));
            return this;
        }
        public String build(){
            results = new StringBuilder();
            for (IsoField v :
                    list) {
                results.append(v.toString());
            }
            return results.toString();
        }

    }

}
