package com.trimindi.pln.inquiryServer.utils.rules.response;



import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class ResponseRulesGeneratorPostPaid {
    public static List<Rules> postPaidInquiryResponse(int bitnumber, boolean success) {
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID",  7));
                R.add(new Rules<Integer>(2, "Subscriber ID",  12));
                R.add(new Rules<Integer>(3, "Bill Status (BS)" , 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(4, "Total Outstanding Bill", 2));
                    R.add(new Rules<String>(5, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(6, "Subscriber Name", 25));
                    R.add(new Rules<String>(7, "Subscriber Unit (SU)", 5));
                    R.add(new Rules<String>(8, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<String>(9, "Subscriber Segmentation", 4));
                    R.add(new Rules<Integer>(10, "Power Consuming Category",  9));
                    R.add(new Rules<Integer>(11, "Total admin Chargers",  9));
                }

                break;
        }
        return R;
    }

    public static List<Rules> postPaidPaymentResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID",  7));
                R.add(new Rules<Integer>(2, "Subscriber ID",  12));
                R.add(new Rules<Integer>(3, "Bill Status (BS)", 1));
                R.add(new Rules<Integer>(4, "PaymentPDAM Status (PS)", 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<Integer>(5, "Total Outstanding Bill", 2));
                    R.add(new Rules<String>(6, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(7, "Subscriber Name", 25));
                    R.add(new Rules<String>(8, "Subscriber Unit (SU)", 5));
                    R.add(new Rules<String>(9, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<String>(10, "Subscriber Segmentation", 4));
                    R.add(new Rules<Integer>(11, "Power Consuming Category", 9));
                    R.add(new Rules<Integer>(12, "Total admin Chargers",  9));
                }
                break;
        }
        return R;
    }

    public static List<Rules> rulesRincian(){
        List<Rules> R = new ArrayList<>();
            R.add(new Rules<Integer>(0, "Bill Period (repeated) ",  6));
            R.add(new Rules<Integer>(1, "Due Date (repeated)",  8));
            R.add(new Rules<Integer>(2, "Meter Read Date (repeated)",  8));
            R.add(new Rules<Integer>(3, "Total Electricity Bill (repeated)",  12));
            R.add(new Rules<String>(4, "Incentive (repeated)",  11));
            R.add(new Rules<Integer>(5, "Value Added Tax* (repeated)", 10));
            R.add(new Rules<Integer>(6, "Penalty Fee (repeated)",  12));
            R.add(new Rules<Integer>(7, "Previous Meter Reading 1 (repeated)",  8));
            R.add(new Rules<Integer>(8, "Current Meter Reading 1 (repeated)", 8));
            R.add(new Rules<Integer>(9, "Previous Meter Reading 2(repeated)",  8));
            R.add(new Rules<Integer>(10, "Current Meter Reading 2 (repeated)",  8));
            R.add(new Rules<Integer>(11, "Previous Meter Reading 3(repeated)",  8));
            R.add(new Rules<Integer>(12, "Current Meter Reading 3 (repeated)",  8));
        return R;
    }
}
