package com.trimindi.pln.inquiryServer.utils.constanta;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by sx on 13/05/17.
 */
@XmlRootElement
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseCode {
    public static final ResponseCode SERVER_UNAVAILABLE = new ResponseCode("9999","TERJADI KESALAHA PADA SERVER");
    public static final ResponseCode SERVER_TIMEOUT = new ResponseCode("9998", "REQUEST TIMOUT");
    public static final ResponseCode PARAMETER_MERCHANT_TYPE = new ResponseCode("9997","MERCHANT CODE TIDAK DIKENALI");
    public static final ResponseCode PARAMETER_SALDO = new ResponseCode("1003","SALDO ANDA TIDAK MENCUKUPI UNTUK MELAKUKAN TRANSAKSI");
    public static final ResponseCode PARAMETER_UNKNOW_DENOM = new ResponseCode("1002","KODE PRODUCT TIDAK DIKENALI");
    public static final ResponseCode NTRANS_NOT_FOUND = new ResponseCode("1006","NO NTRANS TIDAK DI DITEMUKAN");
    public static final ResponseCode MISSING_REQUIRED_PARAMETER = new ResponseCode("8999","PARAMETER TIDAK LENGKAP");
    public static final ResponseCode PAYMENT_UNDER_PROSES = new ResponseCode("1007","PEMBAYARAN SEDANG DALAM PROSES");
    public static final ResponseCode PAYMENT_DONE = new ResponseCode("1008","NTRANS SUDAH TIDAK BERLAKU PEMBAYARAN BERHASIL");
    public static final ResponseCode PAYMENT_FAILED = new ResponseCode("1009","PEMBAYARAN GAGAL ");
    public static final ResponseCode MAXIMUM_PRINTED_RECIPT = new ResponseCode("1010", "MELEBIHI BATAS MAKSIMAL CETAK ULANG");
    public static final ResponseCode WRONG_NTRANS = new ResponseCode("1030", "NO TRANSAKSI TIDAK DI KENALI");
    public static final ResponseCode UNKNOW_ACTION = new ResponseCode("2000", "ACTION TIDAK DI TERDAFTAR");
    public static final ResponseCode INVALID_BODY_REQUEST_FORMAT = new ResponseCode("2001","REQUEST BODY ANDA TIDAK BENAR");
    public static final ResponseCode PRODUCT_UNDER_MAINTANCE = new ResponseCode("2002", "PRODUCT SEDANG DALAM MAINTANCE");
    public static final ResponseCode PARAMETER_INVALID_PRODUCT_ACTION = new ResponseCode("2012", "INVALID ACTION DAN PRODUCT");
    public static final ResponseCode UNAUTHORIZED_REQUEST = new ResponseCode("9997", "UNAUTHORIZATION REQUEST");
    public static final ResponseCode UNREGISTERD_IP_ADDRESS = new ResponseCode("9977", "UNREGISTERED IP ADDRESS");
    public static final ResponseCode NOMINAL_TIDAK_SAMA = new ResponseCode("1111", "NOMINAL TIDAK SAMA DENGAN TAGIHAN");

    private boolean status;
    private String code;
    private String message;
    private String ntrans;
    private String product;
    private String mssidn;

    public ResponseCode() {
    }

    public String getMssidn() {
        return mssidn;
    }

    public ResponseCode setMssidn(String mssidn) {
        this.mssidn = mssidn;
        return this;
    }

    public String getProduct() {
        return product;
    }

    public ResponseCode setProduct(String product) {
        this.product = product;
        return this;
    }

    @XmlElement(name = "ntrans",nillable = false)
    public String getNtrans() {
        return ntrans;
    }

    public ResponseCode setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    public ResponseCode(String code, String message) {
        this.code = code;
        this.status = false;
        this.message = message;
    }

    public boolean isStatus() {
        return status;
    }

    public ResponseCode setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getCode() {
        return code;
    }

    public ResponseCode setCode(String code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseCode setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return "ResponseCode{" +
                "status=" + status +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", ntrans='" + ntrans + '\'' +
                '}';
    }
}
