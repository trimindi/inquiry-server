package com.trimindi.pln.inquiryServer.utils.generator;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.IsoType;
import com.trimindi.pln.inquiryServer.AppProperties;
import com.trimindi.pln.inquiryServer.SimpleTraceGenerator;
import com.trimindi.pln.inquiryServer.client.client.Iso8583Client;
import com.trimindi.pln.inquiryServer.models.Transaksi;
import com.trimindi.pln.inquiryServer.response.postpaid.Inquiry;
import com.trimindi.pln.inquiryServer.response.postpaid.Rincian;
import com.trimindi.pln.inquiryServer.utils.iso.FieldBuilder;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;
import com.trimindi.pln.inquiryServer.utils.iso.parsing.SDE;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorNonTagList;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorPostPaid;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorPrePaid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.trimindi.pln.inquiryServer.utils.generator.BaseHelper.*;

/**
 * Created by PC on 27/07/2017.
 */

@Component
public class MessageGenerator {

    private static final Logger logger = LoggerFactory.getLogger(MessageGenerator.class);
    private final AppProperties config;
    private SimpleTraceGenerator generator;


    @Autowired
    Iso8583Client<IsoMessage> iso8583Client;
    @Autowired
    public MessageGenerator(SimpleTraceGenerator generator, AppProperties appProperties) {
        this.generator = generator;
        this.config = appProperties;
    }

    public IsoMessage signON(){
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2800);
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(33,IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(40,IsoType.ALPHA.value(config.SIGN_ON,3));
        isoMessage.setField(41,IsoType.ALPHA.value(config.TERMINAL_ID,16));
        isoMessage.setField(48, IsoType.LLLLVAR.value(config.SWITCHER_ID));
        return isoMessage;
    }


    public IsoMessage signOFF(){
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2800);
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(33,IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(40,IsoType.NUMERIC.value(config.SIGN_OFF,3));
        isoMessage.setField(41,IsoType.ALPHA.value(config.TERMINAL_ID,16));
        isoMessage.setField(48, IsoType.LLLLVAR.value(config.SWITCHER_ID));
        return isoMessage;
    }

    public IsoMessage echoMSG(){
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2800);
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(33,IsoType.LLVAR.value(config.PARTNER_ID));
        isoMessage.setField(40,IsoType.NUMERIC.value(config.ECHO_TEST,3));
        isoMessage.setField(41,IsoType.ALPHA.value(config.TERMINAL_ID,16));
        isoMessage.setField(48, IsoType.LLLLVAR.value(config.SWITCHER_ID));
        return isoMessage;
    }

    public IsoMessage inquiryPostpaid(String MSSIDN,String MT,String TID,String PID){
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2100);
        isoMessage.setField(2,IsoType.LLVAR.value(config.PAN_POSTPAID));
        isoMessage.setField(11,IsoType.ALPHA.value(generator.nextTrace(),12));
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(26,IsoType.ALPHA.value(MT,4));
        isoMessage.setField(32,IsoType.LLVAR.value(config.BANK_CODE));
        isoMessage.setField(33,IsoType.LLVAR.value(PID));
        isoMessage.setField(41, IsoType.ALPHA.value(TID,16));
        isoMessage.setField(48,IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID,7,"","L")
                .addValue(MSSIDN,12,"0","R")
                .addValue("1",1,"0","R")
                .build()));
        return isoMessage;
    }

    public IsoMessage paymentPostpaid(Transaksi transaksi) {
        String payload = transaksi.getINQUIRY();
        IsoMessage inquiry = null;
        try {
            inquiry = iso8583Client.getIsoMessageFactory().parseMessage(payload.getBytes(),0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .generate();

        Inquiry inquiryResponse = new Inquiry(bit48,true);
        FieldBuilder.Builder bit48compose = new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID,7,"","L")
                .addValue(transaksi.getMSSIDN(),12,"0","L")
                .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                .addValue(String.valueOf(inquiryResponse.getTotalOutstandingBill()),2,"0","L")
                .addValue(inquiryResponse.getBukopinTbkReferenceNumber(),32," ","R")
                .addValue(inquiryResponse.getSubscriberName(),25," ","R")
                .addValue(inquiryResponse.getSubscriberUnit(), 5, "0", "L")
                .addValue(inquiryResponse.getServiceUnitPhone(), 15, "0", "L")
                .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R")
                .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L")
                .addValue(new DecimalFormat("#").format(inquiryResponse.getAdmin()),9,"0","L");

        int legth = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .calculate();

        String rincian = inquiry.getObjectValue(48).toString().substring(legth,inquiry.getObjectValue(48).toString().length());
        List<Rincian> rincians = new ArrayList<>();
        int start = 0;
        int leghtRincian = 115;
        double total = 0;
        double denda = 0;
        for(int i= 0;i<inquiryResponse.getBillStatus();i++){
            String parsRincian = rincian.substring(start,start+leghtRincian);
            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
            Rincian r = new Rincian(rc);
            rincians.add(r);
            total += r.getTotalElectricityBill();
            denda += r.getPenaltyFee();
            start += leghtRincian;
            bit48compose.addValue(parsRincian,115,"0","L");
        }
        total += denda + inquiryResponse.getAdmin();
        inquiryResponse.setRincian(rincians);
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2200);
        isoMessage.setField(2,IsoType.LLVAR.value(config.PAN_POSTPAID));
        isoMessage.setField(4,IsoType.ALPHA.value(amountToPay(total),16));
        isoMessage.setField(11,IsoType.ALPHA.value(inquiry.getObjectValue(11),12));
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(15,IsoType.ALPHA.value(date8(),8));
        isoMessage.setField(26,IsoType.ALPHA.value(transaksi.getMERCHANT_ID(),4));
        isoMessage.setField(32,IsoType.LLVAR.value(config.BANK_CODE));
        isoMessage.setField(33,IsoType.LLVAR.value(inquiry.getObjectValue(33)));
        isoMessage.setField(41, IsoType.ALPHA.value(inquiry.getObjectValue(41),16));
        isoMessage.setField(48,IsoType.LLLVAR.value(bit48compose.build()));
        isoMessage.setField(61,IsoType.LLLVAR.value(transaksi.getBILL_REF_NUMBER()));
        return isoMessage;
    }

    public IsoMessage revesalPostpaid(IsoMessage isoMsg, int MTI) {
        IsoMessage newMsg = isoMsg;
        newMsg.setType(MTI);
        newMsg.update(12, IsoType.ALPHA.value(date14(),14));
        newMsg.update(56, IsoType.LLVAR.value(new FieldBuilder.Builder()
                .addValue(String.valueOf(isoMsg.getType()), 4, "0", "L")
                .addValue(isoMsg.getObjectValue(11), 12, "0", "L")
                .addValue(isoMsg.getObjectValue(12), 14, "0", "L")
                .addValue(isoMsg.getObjectValue(32), 7, "0", "L")
                .build()));
        newMsg.removeFields(61);
        return newMsg;
    }

    public IsoMessage revesalNontaglist(IsoMessage isoMsg, int MTI) {
        IsoMessage newMsg = isoMsg;
        newMsg.setType(MTI);
        newMsg.update(12, IsoType.ALPHA.value(date14(),14));
        newMsg.update(56, IsoType.LLVAR.value(new FieldBuilder.Builder()
                .addValue(String.valueOf(newMsg.getType()), 4, "0", "L")
                .addValue(newMsg.getObjectValue(11), 12, "0", "L")
                .addValue(newMsg.getObjectValue(12), 14, "0", "L")
                .addValue(newMsg.getObjectValue(32), 7, "0", "L")
                .build()));
        newMsg.removeFields(61);
        return newMsg;
    }

    public IsoMessage inquiryPrepaid(String MERCHANT_CODE, String MSSIDN,String TID,String PID) {
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2100);
        isoMessage.setField(2,IsoType.LLVAR.value(config.PAN_PREPAID));
        isoMessage.setField(11,IsoType.ALPHA.value(generator.nextTrace(),12));
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(26,IsoType.ALPHA.value(MERCHANT_CODE,4));
        isoMessage.setField(32,IsoType.LLVAR.value(config.BANK_CODE));
        isoMessage.setField(33,IsoType.LLVAR.value(PID));
        isoMessage.setField(41,IsoType.ALPHA.value(TID,16));
        isoMessage.setField(48,IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID,7,"0","R")
                .addValue((MSSIDN.length() == 11 ? MSSIDN: "0"),11,"0","R")
                .addValue((MSSIDN.length() == 12 ? MSSIDN: "0"),12,"0","R")
                .addValue((MSSIDN.length() == 11 ? "0": "1"),1,"0","R")
                .build()));
        return isoMessage;
    }

    public IsoMessage paymentPrepaid(Transaksi t,int type) {
        String payload = t.getINQUIRY();
        IsoMessage inquiry = null;
        try {
            inquiry = iso8583Client.getIsoMessageFactory().parseMessage(payload.getBytes(),0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(48))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48,true))
                .generate();

        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(62))
                .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62,true))
                .generate();
        bit48.addAll(bit62);
        com.trimindi.pln.inquiryServer.response.prepaid.Inquiry inquiryResponse = new com.trimindi.pln.inquiryServer.response.prepaid.Inquiry(bit48,true);

        String unsold = "0";
        StringBuilder unsoldResponse = new StringBuilder();
        if(inquiryResponse.getTotalRepeat() > 0){
            String data = inquiry.getObjectValue(62);
            int legth = data.length();
            for(int i=0;i<inquiryResponse.getTotalRepeat();i++){
                int newlegth = legth - 11;
                if(t.getAMOUT() == Double.parseDouble(data.substring(newlegth,legth))){
                    unsold = "1";
                }
                unsoldResponse.append(data.substring(newlegth,legth));
                legth = newlegth;
            }
        }
        inquiry.setType(type);
        inquiry.update(4,IsoType.ALPHA.value(amountToPay(t.getAMOUT() + t.getADMIN()),16));
        inquiry.update(12,IsoType.ALPHA.value(date14(),14));
        inquiry.update(48,IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID,7,"","L")
                .addValue(inquiryResponse.getMeterSerialNumber(),11,"0","R")
                .addValue(inquiryResponse.getSubscriberID(),12,"0","R")
                .addValue((t.getMSSIDN().length() == 11? "1" : "0"),1,"0","R")
                .addValue(inquiryResponse.getPLNReferenceNumber(),32," ","R")//ref pln
                .addValue(inquiryResponse.getBukopinReferenceNumber(),32," ","R")//ref bukopin
                .addValue(inquiryResponse.getSubscriberName(),25," ","R")//subscribe name
                .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R") //subscribe segement
                .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L") //power consumstion
                .addValue("0",1,"0","R") // minor unit
                .addValue(numberToPay(inquiryResponse.getAdmin()),10,"0","L") // admin charge
                .addValue(unsold,1,"0","L")
                .build()));
        inquiry.update(62,IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(inquiryResponse.getDistributionCode(),2," ","R")
                .addValue(String.valueOf(inquiryResponse.getServiceUnit()),5,"0","L")
                .addValue(String.valueOf(inquiryResponse.getServiceUnitPhone()),15,"0","L")
                .addValue(String.valueOf(inquiryResponse.getMaxKWHLimit()),5,"0","L")
                .addValue(String.valueOf(inquiryResponse.getTotalRepeat()),1,"0","R")
                .addValue(unsoldResponse.toString(),(inquiryResponse.getTotalRepeat() * 11),"0","L")
                .build()));
        return inquiry;
    }

    public IsoMessage inquiryNontaglist(String MSSIDN, String MT, String TID, String PID) {
        IsoMessage isoMessage = new IsoMessage();
        isoMessage.setType(0x2100);
        isoMessage.setField(2,IsoType.LLVAR.value(config.PAN_NONTAGLIST));
        isoMessage.setField(11,IsoType.ALPHA.value(generator.nextTrace(),12));
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(26,IsoType.ALPHA.value(MT,4));
        isoMessage.setField(32,IsoType.LLVAR.value(config.BANK_CODE));
        isoMessage.setField(33,IsoType.LLVAR.value(PID));
        isoMessage.setField(41,IsoType.ALPHA.value(TID,16));
        isoMessage.setField(48,IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID,7,"0","L")
                .addValue(MSSIDN,32," ","R")
                .addValue("00",2,"0","L")
                .addValue("",3,"0","R")
                .build()));
        return isoMessage;
    }

    public IsoMessage paymentNontaglist(Transaksi transaksi) {
        IsoMessage inquiry = null;
        try {
            inquiry = iso8583Client.getIsoMessageFactory().parseMessage(transaksi.getINQUIRY().getBytes(),0);
        } catch (ParseException | UnsupportedEncodingException e) {
            logger.error(e.getMessage());
        }
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(48))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(48, true))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(62))
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(62, true))
                .generate();
        bit48.addAll(bit62);
        com.trimindi.pln.inquiryServer.response.nontaglist.Inquiry inquiryResponse = new com.trimindi.pln.inquiryServer.response.nontaglist.Inquiry(bit48,true);
        inquiry.setType(0x2200);
        inquiry.update(4,IsoType.ALPHA.value(amountToPay(transaksi.getAMOUT() + transaksi.getADMIN()),16));
        inquiry.update(12,IsoType.ALPHA.value(date14(),14));
        inquiry.update(48,IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID,7,"0","L")
                .addValue(inquiryResponse.getRegistrationNumber(),32," ","R")
                .addValue(inquiryResponse.getAreaCode(),2,"0","L")
                .addValue(inquiryResponse.getTransactionCode(),3,"0","L")
                .addValue(inquiryResponse.getTransactionName(),25," ","R")
                .addValue(inquiryResponse.getRegistrationDate(),8," ","R")
                .addValue(inquiryResponse.getExpirationDate(),8," ","R")
                .addValue(inquiryResponse.getSubscriberID(), 12, "0", "L")
                .addValue(inquiryResponse.getSubscriberName(), 25, " ", "R")
                .addValue(inquiryResponse.getPLNReferenceNumber(),32," ","R")
                .addValue(inquiryResponse.getBukopinReferenceNumber(),32," ","R")
                .addValue(inquiryResponse.getServiceUnit(),5,"0","L")
                .addValue(inquiryResponse.getServiceUnitAddress(),35," ","R")
                .addValue(inquiryResponse.getServiceUnitPhone(),15," ","R")
                .addValue(String.valueOf(inquiryResponse.getTotalTransactionAmountMinorUnit()),1,"0","L")
                .addValue(valueToMinor(inquiryResponse.getTagihan() + inquiryResponse.getAdmin(), inquiryResponse.getTotalTransactionAmountMinorUnit()), 17, "0", "L")
                .addValue(String.valueOf(inquiryResponse.getPLNBILLMinorUnit()),1,"0","L")
                .addValue(valueToMinor(inquiryResponse.getPLNBILLValue(),inquiryResponse.getPLNBILLMinorUnit()),17,"0","L")
                .addValue(String.valueOf(inquiryResponse.getAdministrationChargeMinorUnit()),1,"0","L")
                .addValue(valueToMinor(inquiryResponse.getAdmin(),inquiryResponse.getAdministrationChargeMinorUnit()),10,"0","L")
                .build()));
        inquiry.update(62,IsoType.LLLVAR.value(new FieldBuilder.Builder()
                .addValue(inquiryResponse.getBillcomponenttype(),2,"0","L")
                .addValue(String.valueOf(inquiryResponse.getBillComponentMinorUnit()),1,"0","L")
                .addValue(valueToMinor(inquiryResponse.getBillComponentValueAmount(),inquiryResponse.getBillComponentMinorUnit()),17,"0","L")
                .build()));
        return inquiry;
    }

    public IsoMessage paymentPostpaid(IsoMessage message,Transaksi transaksi) {

        IsoMessage inquiry = message;
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .generate();

        Inquiry inquiryResponse = new Inquiry(bit48,true);
        FieldBuilder.Builder bit48compose = new FieldBuilder.Builder()
                .addValue(config.SWITCHER_ID,7,"","L")
                .addValue(transaksi.getMSSIDN(),12,"0","L")
                .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                .addValue(String.valueOf(inquiryResponse.getBillStatus()),1,"0","R")
                .addValue(String.valueOf(inquiryResponse.getTotalOutstandingBill()),2,"0","L")
                .addValue(inquiryResponse.getBukopinTbkReferenceNumber(),32," ","R")
                .addValue(inquiryResponse.getSubscriberName(),25," ","R")
                .addValue(inquiryResponse.getSubscriberUnit(), 5, "0", "L")
                .addValue(inquiryResponse.getServiceUnitPhone(), 15, "0", "L")
                .addValue(inquiryResponse.getSubscriberSegmentation(),4," ","R")
                .addValue(String.valueOf(inquiryResponse.getPowerConsumingCategory()),9,"0","L")
                .addValue(new DecimalFormat("#").format(inquiryResponse.getAdmin()),9,"0","L");

        int legth = new SDE.Builder()
                .setPayload(inquiry.getObjectValue(48))
                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48,true))
                .calculate();

        String rincian = inquiry.getObjectValue(48).toString().substring(legth,inquiry.getObjectValue(48).toString().length());
        List<Rincian> rincians = new ArrayList<>();
        int start = 0;
        int leghtRincian = 115;
        double total = 0;
        double denda = 0;
        for(int i= 0;i<inquiryResponse.getBillStatus();i++){
            String parsRincian = rincian.substring(start,start+leghtRincian);
            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
            Rincian r = new Rincian(rc);
            rincians.add(r);
            total += r.getTotalElectricityBill();
            denda += r.getPenaltyFee();
            start += leghtRincian;
            bit48compose.addValue(parsRincian,115,"0","L");
        }
        total += denda + inquiryResponse.getAdmin();
        inquiryResponse.setRincian(rincians);
        IsoMessage isoMessage = iso8583Client.getIsoMessageFactory().newMessage(0x2200);
        isoMessage.setField(2,IsoType.LLVAR.value(config.PAN_POSTPAID));
        isoMessage.setField(4,IsoType.ALPHA.value(amountToPay(total),16));
        isoMessage.setField(11,IsoType.ALPHA.value(inquiry.getObjectValue(11),12));
        isoMessage.setField(12,IsoType.ALPHA.value(date14(),14));
        isoMessage.setField(15,IsoType.ALPHA.value(date8(),8));
        isoMessage.setField(26,IsoType.ALPHA.value(transaksi.getMERCHANT_ID(),4));
        isoMessage.setField(32,IsoType.LLVAR.value(config.BANK_CODE));
        isoMessage.setField(33,IsoType.LLVAR.value(inquiry.getObjectValue(33)));
        isoMessage.setField(41, IsoType.ALPHA.value(inquiry.getObjectValue(41),16));
        isoMessage.setField(48,IsoType.LLLVAR.value(bit48compose.build()));
        isoMessage.setField(61,IsoType.LLLVAR.value(transaksi.getBILL_REF_NUMBER()));
        return isoMessage;
    }
}
