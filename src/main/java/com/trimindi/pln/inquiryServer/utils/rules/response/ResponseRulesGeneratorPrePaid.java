package com.trimindi.pln.inquiryServer.utils.rules.response;



import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class ResponseRulesGeneratorPrePaid {
    public static List<Rules> prePaidInquiryResponse(int bitnumber, boolean success) {
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Meter Serial Number", 11));
                R.add(new Rules<Integer>(3, "Subscriber ID", 12));
                R.add(new Rules<Integer>(4, "Flag", 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){

                    R.add(new Rules<String>(5, "PLN Reference Number", 32));
                    R.add(new Rules<String>(6, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<String>(7, "Subscriber Name", 25));
                    R.add(new Rules<String>(8, "Subscriber Segmentation", 4));
                    R.add(new Rules<Integer>(9, "Power Consuming Category", 9));
                    R.add(new Rules<Integer>(10, "Minor Unit of Admin Charge", 1));
                    R.add(new Rules<Integer>(11, "Admin Charge (AC)", 10));
                }

                break;
            case 62:
                R.clear();
                if(success){
                    R.add(new Rules<Integer>(1, "Distribution Code", 2));
                    R.add(new Rules<String>(2, "Service Unit (SU)", 5));
                    R.add(new Rules<Integer>(3, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<Integer>(4, "Max KWH Limit", 5));
                    R.add(new Rules<Integer>(5, "Total Repeat", 1));
//                    R.add(new Rules<Integer>(6, "Power Purchase Unsold (PP) (repeated)", 11));
                }
                break;
        }
        return R;
    }

    public static List<Rules> prePaidPaymentResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Meter Serial Number", 11));
                R.add(new Rules<Integer>(3, "Subscriber ID", 12));
                R.add(new Rules<Integer>(4, "Flag", 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){
                    R.add(new Rules<String>(5, "PLN Reference Number", 32));
                    R.add(new Rules<String>(6, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<Integer>(7, "Vending Receipt Number", 8));
                    R.add(new Rules<String>(8, "Subscriber Name", 25));
                    R.add(new Rules<String>(9, "Subscriber Segmentation", 4));
                    R.add(new Rules<Integer>(10, "Power Consuming Category", 9));
                    R.add(new Rules<Integer>(11, "Buying Option", 1));
                    R.add(new Rules<Integer>(12, "Minor Unit of Admin Charge", 1));
                    R.add(new Rules<Integer>(13, "Admin Charge (AC)", 10));
                    R.add(new Rules<Integer>(14, "Minor Unit of Stamp Duty", 1));
                    R.add(new Rules<Integer>(15, "Stamp Duty (SD)", 10));
                    R.add(new Rules<Integer>(16, "Minor Unit of Value Added Tax", 1));
                    R.add(new Rules<Integer>(17, "Value Added Tax (VTx)", 10));
                    R.add(new Rules<Integer>(18, "Minor Unit of Public Lighting Tax", 1));
                    R.add(new Rules<Integer>(19, "Public Lighting Tax (PTx)", 10));
                    R.add(new Rules<Integer>(20, "Minor Unit of Customer Payables Installment", 1));
                    R.add(new Rules<Integer>(21, "Customer Payables Installment (PI)", 10));
                    R.add(new Rules<Integer>(22, "Minor Unit of Power Purchase", 1));
                    R.add(new Rules<Integer>(23, "Power Purchase (PP)", 12));
                    R.add(new Rules<Integer>(24, "Minor Unit of Purchased KWH Unit", 1));
                    R.add(new Rules<Integer>(25, "Purchased KWH Unit (KWH)", 10));
                    R.add(new Rules<Integer>(26, "Token Number", 20));
                }


                break;
            case 62:
                R.clear();
                if(success){
                    R.add(new Rules<Integer>(1, "Distribution Code", 2));
                    R.add(new Rules<String>(2, "Service Unit (SU)", 5));
                    R.add(new Rules<Integer>(3, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<Integer>(4, "Max KWH Limit", 5));
                    R.add(new Rules<Integer>(5, "Total Repeat", 1));
                }
                break;
        }
        return R;
    }

    public static List<Rules> prePaidPaymentAdvceResponse(int bitnumber,boolean success){
        List<Rules> R = new ArrayList<>();
        switch (bitnumber){
            case 48:
                R.clear();
                R.add(new Rules<String>(1, "Switcher ID", 7));
                R.add(new Rules<Integer>(2, "Meter Serial Number", 11));
                R.add(new Rules<Integer>(3, "Subscriber ID", 12));
                R.add(new Rules<Integer>(4, "Flag", 1));
                /**
                 * IF RC 0000 = SUCCESS
                 */
                if(success){

                    R.add(new Rules<String>(5, "PLN Reference Number", 32));
                    R.add(new Rules<String>(6, "PT.Bank Bukopin, Tbk Reference Number", 32));
                    R.add(new Rules<Integer>(7, "Vending Receipt Number", 8));
                    R.add(new Rules<String>(8, "Subscriber Name", 25));
                    R.add(new Rules<String>(9, "Subscriber Segmentation", 4));
                    R.add(new Rules<Integer>(10, "Power Consuming Category", 9));
                    R.add(new Rules<Integer>(11, "Buying Option", 1));
                    R.add(new Rules<Integer>(12, "Minor Unit of Admin Charge", 1));
                    R.add(new Rules<Integer>(13, "Admin Charge (AC)", 10));
                    R.add(new Rules<Integer>(14, "Minor Unit of Stamp Duty", 1));
                    R.add(new Rules<Integer>(15, "Stamp Duty (SD)", 10));
                    R.add(new Rules<Integer>(16, "Minor Unit of Value Added Tax", 1));
                    R.add(new Rules<Integer>(17, "Value Added Tax (VTx)", 10));
                    R.add(new Rules<Integer>(18, "Minor Unit of Public Lighting Tax", 1));
                    R.add(new Rules<Integer>(19, "Public Lighting Tax (PTx)", 10));
                    R.add(new Rules<Integer>(20, "Minor Unit of Customer Payables Installment", 1));
                    R.add(new Rules<Integer>(21, "Customer Payables Installment (PI)", 10));
                    R.add(new Rules<Integer>(22, "Minor Unit of Power Purchase", 1));
                    R.add(new Rules<Integer>(23, "Power Purchase (PP)", 12));
                    R.add(new Rules<Integer>(24, "Minor Unit of Purchased KWH Unit", 1));
                    R.add(new Rules<Integer>(25, "Purchased KWH Unit (KWH)", 10));
                    R.add(new Rules<Integer>(26, "Token Number", 20));
                }

                break;
            case 62:
                R.clear();
                if(success){
                    R.add(new Rules<Integer>(1, "Distribution Code", 2));
                    R.add(new Rules<String>(2, "Service Unit (SU)", 5));
                    R.add(new Rules<Integer>(3, "Service Unit Phone (SUP)", 15));
                    R.add(new Rules<Integer>(4, "Max KWH Limit", 5));
                    R.add(new Rules<Integer>(5, "Total Repeat", 1));
                    R.add(new Rules<Integer>(6, "Power Purchase Unsold (PP) (repeated)", 11));
                }
                break;
        }
        return R;
    }


}
