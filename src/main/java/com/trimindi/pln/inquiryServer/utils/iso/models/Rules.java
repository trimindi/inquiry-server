package com.trimindi.pln.inquiryServer.utils.iso.models;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Comparator;

/**
 * Created by HP on 14/05/2017.
 */

@XmlRootElement
public class Rules<T> implements Comparator<Rules> {
    @XmlTransient
    public static final int TYPE_STRING = 1;
    @XmlTransient
    public static final int TYPE_DOUBLE = 2;
    @XmlTransient
    public static final int TYPE_INTEGER = 3;

    @XmlTransient
    private int start;
    @XmlTransient
    private int length;
    @XmlTransient
    private int index;
    private String field;
    private T results;
    @XmlTransient
    private Class<T> type;

    public Rules(T msg){
        this.results = msg;
    }
    public Rules(int index, String field, int start, int length) {
        this.start = start;
        this.length = length;
        this.field = field;
        this.index = index;
    }
    public Rules(int index, String field, int length) {
        this.start = 1000;
        this.length = length;
        this.field = field;
        this.index = index;
    }

    @Override
    public String toString() {
        return "Rules{" +
                "field='" + field + '\'' +
                ", results=" + results +
                '}';
    }

    public T getResults() {
        return (T) ((String) results).trim();
    }

    public void setResults(T results) {
        this.results = results;
    }

    @XmlTransient
    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    @XmlTransient
    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @XmlTransient
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public int compare(Rules o1, Rules o2) {
        int index1 = o1.getIndex();
        int index2 = o2.getIndex();

        if (index1 == index2)
            return 0;
        else if (index1 > index2)
            return 1;
        else
            return -1;
    }
}
