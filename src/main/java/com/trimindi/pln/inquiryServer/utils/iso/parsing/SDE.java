package com.trimindi.pln.inquiryServer.utils.iso.parsing;



import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HP on 14/05/2017.
 */
public class SDE {
    private int payloadlength = 0;
    private int length = 0;
    private String payload;
    private List<Rules> rules;

    public SDE(List<Rules> rules, String payload) {
        this.rules = rules;
        this.payload = payload;
        this.payloadlength = payload.length();
    }

    public int calculate() {
        length = 0;
        for (Rules r : rules) {
            length += r.getLength();
        }
        return length;
    }

    public List<Rules> generate() {
        calculate();
        List<Rules> R = new ArrayList<>();
        int pos = 0;
        for (Rules r : rules) {
            if(r.getStart() == 1000){
                if(r.getLength() > (length - pos)){
                    break;
                }
                r.setResults(payload.substring(pos, pos + r.getLength()));
            }else{
                r.setResults(payload.substring(r.getStart(),r.getStart() + r.getLength()));
            }
            pos += r.getLength();
            R.add(r);
        }
        return R;
    }

    public static class Builder {
        private String payload;
        private List<Rules> rules;

        public Builder() {

        }

        public Builder setPayload(String payload) {
            this.payload = payload;
            return this;
        }

        public Builder setRules(List<Rules> rules) {
            this.rules = rules;
            return this;
        }

        public List<Rules> generate() {
            return new SDE(rules, payload).generate();
        }

        public int calculate() {
            return new SDE(rules, payload).calculate();
        }
    }


}
