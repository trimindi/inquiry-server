package com.trimindi.pln.inquiryServer.utils.iso.parsing;


import com.solab.iso8583.IsoMessage;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorNonTagList;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorPostPaid;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorPrePaid;

import java.util.List;

/**
 * Created by HP on 23/05/2017.
 */
public class ParsingHelper {
    public static List<Rules> parsingRulesPostPaid(IsoMessage d, boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getField(48).getCharacterEncoding())
                .setRules(ResponseRulesGeneratorPostPaid.postPaidPaymentResponse(48, status))
                .generate();
        bit48.add(new Rules(d.getField(63).getCharacterEncoding()));
        return bit48;
    }
    public static List<Rules> parsingRulesPrepaid(IsoMessage d, boolean status) {
        List<Rules> bit48 = null;
        try{
            bit48 = new SDE.Builder().setPayload(d.getField(48).toString())
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(48,status))
                    .generate();
            List<Rules> bit62 = new SDE.Builder()
                    .setPayload(d.getField(62).toString())
                    .setRules(ResponseRulesGeneratorPrePaid.prePaidPaymentResponse(62,status))
                    .generate();
            bit48.addAll(bit62);
            bit48.add(new Rules(d.getField(63).toString()));
            return bit48;
        }catch (Exception e){
            e.printStackTrace();
        }
        return bit48;
    }
    public static List<Rules> parsingRulesNontaglist(IsoMessage d, boolean status) {
        List<Rules> bit48 = new SDE.Builder()
                .setPayload(d.getField(48).toString())
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(48, status))
                .generate();
        List<Rules> bit61 = new SDE.Builder()
                .setPayload(d.getField(61).toString())
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(61, status))
                .generate();
        List<Rules> bit62 = new SDE.Builder()
                .setPayload(d.getField(62).toString())
                .setRules(ResponseRulesGeneratorNonTagList.nonTagListPaymentResponse(62, status))
                .generate();
        bit48.addAll(bit61);
        bit48.addAll(bit62);
        bit48.add(new Rules(d.getField(63).toString()));
        return bit48;
    }
}
