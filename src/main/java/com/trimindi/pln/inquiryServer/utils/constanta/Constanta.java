package com.trimindi.pln.inquiryServer.utils.constanta;

/**
 * Created by HP on 20/05/2017.
 */
public class Constanta {
    public static final String MAC = "MAC";
    public static final String LATITUDE = "LATITUDE";
    public static final String LONGITUDE = "LONGITUDE";
    public static final String IP_ADDRESS = "IP ADDRESS";
    public static final String PRINCIPAL = "principal";
    public static final String DENOM = "PRODUCT";
    public static final String MACHINE_TYPE = "MT";
    public static final String MSSIDN = "MSSIDN";
    public static final String NTRANS = "NTRANS";
    public static final String PRODUCT_CODE = "product";
    public static final String AREA = "AREA";
    public static final String INSTANSI = "INSTANSI";
    public static final String BACK_LINK = "BL";
    public static final String PERMISION = "PARTNER_PERMISION";
    public static final String TID = "TID";
    public static final String PID = "PID";
}
