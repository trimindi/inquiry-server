package com.trimindi.pln.inquiryServer.services;

import com.trimindi.pln.inquiryServer.models.PartnerCredential;
import com.trimindi.pln.inquiryServer.repository.PartnerCredentialRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional
public class PartnerCredentialImpl implements PartnerCredentialService {

    PartnerCredentialRepository partnerCredentialRepository;

    public PartnerCredentialImpl(PartnerCredentialRepository partnerCredentialRepository) {
        this.partnerCredentialRepository = partnerCredentialRepository;
    }

    @Override
    public PartnerCredential findById(String id) {
        return partnerCredentialRepository.findOne(id);
    }

    @Override
    public List<PartnerCredential> findAll() {
        return partnerCredentialRepository.findAll();
    }

    @Override
    public PartnerCredential save(PartnerCredential values) {
        return partnerCredentialRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        partnerCredentialRepository.delete(value);
    }

    @Override
    public void update(PartnerCredential values) {
        partnerCredentialRepository.save(values);
    }

}
