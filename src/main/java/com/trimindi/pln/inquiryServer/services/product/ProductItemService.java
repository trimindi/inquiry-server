package com.trimindi.pln.inquiryServer.services.product;

import com.trimindi.pln.inquiryServer.models.ProductItem;
import com.trimindi.pln.inquiryServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */

public interface ProductItemService extends BaseService<ProductItem, String> {
}
