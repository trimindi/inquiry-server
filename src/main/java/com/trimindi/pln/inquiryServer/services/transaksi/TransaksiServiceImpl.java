package com.trimindi.pln.inquiryServer.services.transaksi;

import com.trimindi.pln.inquiryServer.models.Transaksi;
import com.trimindi.pln.inquiryServer.repository.TransaksiRepository;
import com.trimindi.pln.inquiryServer.utils.constanta.TStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional
public class TransaksiServiceImpl implements TransaksiService {


    private TransaksiRepository transaksiRepository;

    public TransaksiServiceImpl(TransaksiRepository transaksiRepository) {
        this.transaksiRepository = transaksiRepository;
    }

    @Override
    public Transaksi findById(String id) {
        return transaksiRepository.findOne(id);
    }

    @Override
    public List<Transaksi> findAll() {
        return transaksiRepository.findAll();
    }

    @Override
    public Transaksi save(Transaksi values) {
        return transaksiRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        transaksiRepository.delete(value);
    }

    @Override
    public void update(Transaksi values) {
        transaksiRepository.save(values);
    }

    @Override
    public Double findTotalFeePartner(String partner) {
        return transaksiRepository.findTotalFee(partner);
    }

    @Override
    public Boolean isTransaksiReadyToPay(String ntrans) {
        Transaksi transaksi = transaksiRepository.findOne(ntrans);
        switch (transaksi.getST()) {
            case TStatus.INQUIRY:
                return true;
            case TStatus.PAYMENT_FAILED:
                return false;
            case TStatus.PAYMENT_SUCCESS:
                return false;
            case TStatus.PAYMENT_PROSESS:
                return false;
        }
        return false;
    }

    @Override
    public Transaksi findByHostReffNumber(String reg_id) {
        return transaksiRepository.findByHOST_REF_NUMBER(reg_id);
    }

}
