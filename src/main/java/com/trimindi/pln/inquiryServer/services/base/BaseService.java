package com.trimindi.pln.inquiryServer.services.base;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
public interface BaseService<T, V> {
    T findById(V id);

    List<T> findAll();

    T save(T values);

    void deleteById(V value);

    void update(T values);
}
