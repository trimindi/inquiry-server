package com.trimindi.pln.inquiryServer.services.transaksi;

import com.trimindi.pln.inquiryServer.models.Transaksi;
import com.trimindi.pln.inquiryServer.services.base.BaseService;

/**
 * Created by PC on 10/08/2017.
 */

public interface TransaksiService extends BaseService<Transaksi, String> {
    Double findTotalFeePartner(String partner);

    Boolean isTransaksiReadyToPay(String ntrans);

    Transaksi findByHostReffNumber(String reg_id);
}
