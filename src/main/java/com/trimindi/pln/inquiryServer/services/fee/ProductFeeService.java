package com.trimindi.pln.inquiryServer.services.fee;

import com.trimindi.pln.inquiryServer.models.ProductFee;
import com.trimindi.pln.inquiryServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface ProductFeeService extends BaseService<ProductFee, String> {
    ProductFee findFeePartner(String partner, String denom);
}
