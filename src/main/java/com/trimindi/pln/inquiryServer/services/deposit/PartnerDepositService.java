package com.trimindi.pln.inquiryServer.services.deposit;

import com.trimindi.pln.inquiryServer.models.PartnerDeposit;
import com.trimindi.pln.inquiryServer.models.Transaksi;
import com.trimindi.pln.inquiryServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface PartnerDepositService extends BaseService<PartnerDeposit, String> {
    boolean bookingSaldo(Transaksi transaksi);

    void reverseSaldo(Transaksi transaksi, String response);
}
