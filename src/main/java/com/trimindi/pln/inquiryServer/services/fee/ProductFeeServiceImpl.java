package com.trimindi.pln.inquiryServer.services.fee;

import com.trimindi.pln.inquiryServer.models.ProductFee;
import com.trimindi.pln.inquiryServer.repository.ProductPriceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional(readOnly = true)
public class ProductFeeServiceImpl implements ProductFeeService {


    private final ProductPriceRepository productProceRepository;

    public ProductFeeServiceImpl(ProductPriceRepository productPriceRepository) {
        this.productProceRepository = productPriceRepository;
    }

    @Override
    public ProductFee findById(String id) {
        return productProceRepository.findOne(id);
    }

    @Override
    public List<ProductFee> findAll() {
        return productProceRepository.findAll();
    }

    @Override
    public ProductFee save(ProductFee values) {
        return productProceRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        productProceRepository.delete(value);
    }

    @Override
    public void update(ProductFee values) {
        productProceRepository.save(values);
    }

    @Override
    public ProductFee findFeePartner(String partner, String denom) {
        return productProceRepository.findByPrice(partner, denom);
    }
}
