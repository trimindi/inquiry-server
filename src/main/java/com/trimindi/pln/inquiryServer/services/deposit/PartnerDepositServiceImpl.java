package com.trimindi.pln.inquiryServer.services.deposit;

import com.trimindi.pln.inquiryServer.models.PartnerDeposit;
import com.trimindi.pln.inquiryServer.models.Transaksi;
import com.trimindi.pln.inquiryServer.repository.PartnerDepositRepository;
import com.trimindi.pln.inquiryServer.services.transaksi.TransaksiService;
import com.trimindi.pln.inquiryServer.utils.constanta.TStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional
public class PartnerDepositServiceImpl implements PartnerDepositService {

    private final PartnerDepositRepository partnerDepositRepository;
    private final TransaksiService transaksiService;

    public PartnerDepositServiceImpl(
            PartnerDepositRepository partnerDepositRepository,
            TransaksiService transaksiService) {
        this.transaksiService = transaksiService;
        this.partnerDepositRepository = partnerDepositRepository;
    }

    @Override
    public PartnerDeposit findById(String id) {

        return partnerDepositRepository.findOne(id);
    }

    @Override
    public List<PartnerDeposit> findAll() {
        return partnerDepositRepository.findAll();
    }

    @Override
    public PartnerDeposit save(PartnerDeposit values) {
        return partnerDepositRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        partnerDepositRepository.delete(value);
    }

    @Override
    public void update(PartnerDeposit values) {
        if (partnerDepositRepository.exists(values.getPartner_id())) {
            partnerDepositRepository.save(values);
        }
    }

    @Override
    @Transactional()
    public boolean bookingSaldo(Transaksi t) {
        try {
            PartnerDeposit p = findById(t.getPARTNERID());
            if (p.getBALANCE() < t.getCHARGE()) {
                return false;
            }
            double saldo = p.getBALANCE() - t.getCHARGE();
            p.setMASUK(0.00);
            p.setBALANCE(saldo);
            p.setKELUAR(p.getKELUAR() + t.getCHARGE());
            update(p);
            t.setDEBET(p.getKELUAR() + t.getCHARGE());
            t.setBALANCE(saldo);
            t.setTIME_PAYMENT(new Timestamp(System.currentTimeMillis()));
            t.setST(TStatus.PAYMENT_PROSESS);
            transaksiService.update(t);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public void reverseSaldo(Transaksi t, String response) {
        PartnerDeposit p = findById(t.getPARTNERID());
        double saldo = p.getBALANCE() + t.getCHARGE();
        p.setBALANCE(saldo);
        p.setKELUAR(p.getKELUAR() - t.getCHARGE());
        update(p);
        t.setPAYMENT(response);
        t.setKREDIT(t.getCHARGE());
        t.setST(TStatus.PAYMENT_FAILED);
        transaksiService.update(t);
    }


}
