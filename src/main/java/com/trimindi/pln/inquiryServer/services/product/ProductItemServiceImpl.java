package com.trimindi.pln.inquiryServer.services.product;

import com.trimindi.pln.inquiryServer.models.ProductItem;
import com.trimindi.pln.inquiryServer.repository.ProductItemRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by PC on 12/08/2017.
 */
@Service
@Transactional(readOnly = true)
public class ProductItemServiceImpl implements ProductItemService {

    private final ProductItemRepository productItemRepository;

    public ProductItemServiceImpl(ProductItemRepository productItemRepository) {
        this.productItemRepository = productItemRepository;
    }

    @Override
    public ProductItem findById(String id) {
        return productItemRepository.findOne(id);
    }

    @Override
    public List<ProductItem> findAll() {
        return productItemRepository.findAll();
    }

    @Override
    public ProductItem save(ProductItem values) {
        return productItemRepository.save(values);
    }

    @Override
    public void deleteById(String value) {
        productItemRepository.delete(value);
    }

    @Override
    public void update(ProductItem values) {
        productItemRepository.delete(values);
    }
}
