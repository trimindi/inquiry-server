package com.trimindi.pln.inquiryServer.services;

import com.trimindi.pln.inquiryServer.models.PartnerCredential;
import com.trimindi.pln.inquiryServer.services.base.BaseService;

/**
 * Created by PC on 12/08/2017.
 */
public interface PartnerCredentialService extends BaseService<PartnerCredential, String> {
}
