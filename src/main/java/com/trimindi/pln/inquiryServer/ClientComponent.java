package com.trimindi.pln.inquiryServer;

import com.solab.iso8583.IsoMessage;
import com.solab.iso8583.MessageFactory;
import com.solab.iso8583.parse.ConfigParser;
import com.trimindi.pln.inquiryServer.client.client.ClientConfiguration;
import com.trimindi.pln.inquiryServer.client.client.Iso8583Client;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by PC on 08/08/2017.
 */
@Configuration
public class ClientComponent {

    public static final Logger logger = LoggerFactory.getLogger(ClientComponent.class);

    @Value("${iso8583.connection.host}")
    private String host;

    @Value("${iso8583.connection.port}")
    private int port;

    @Value("${iso8583.connection.idleTimeout}")
    private int idleTimeout;
    @Value("${iso8583.connection.echomessage}")
    private boolean echomessage;

    @Value("${iso8583.connection.logsensitifdata}")
    private boolean logsensitifdata;
    @Value("${iso8583.connection.replayonerror}")
    private boolean replayonerror;

    @Bean
    public Iso8583Client<IsoMessage> iso8583Client(MessageFactory<IsoMessage> messageFactory) throws IOException {
        SocketAddress socketAddress = new InetSocketAddress(host, port);
        final ClientConfiguration configuration = ClientConfiguration.newBuilder()
                .withIdleTimeout(45)
                .withSensitiveDataFields()
                .withEchoMessageListener(false)
                .withLogSensitiveData(false)
                .withAddLoggingHandler(true)
                .withReplyOnError(false)
                .build();

        return new Iso8583Client<>(socketAddress, configuration, messageFactory);
    }

    @Bean
    public MessageFactory<IsoMessage> clientMessageFactory() throws IOException {
        final MessageFactory<IsoMessage> messageFactory = ConfigParser.createDefault();
        messageFactory.setCharacterEncoding(StandardCharsets.UTF_8.name());
        messageFactory.setUseBinaryMessages(false);
        messageFactory.setAssignDate(false);
        messageFactory.setForceSecondaryBitmap(false);
        messageFactory.setIgnoreLastMissingField(true);
        messageFactory.setUseBinaryBitmap(false);
        return messageFactory;
    }

    @Bean
    public CloseableHttpClient closeableHttpClient(SSLContext sslContext){
        CloseableHttpClient client = HttpClients.custom()
                    .setSSLContext(sslContext)
                    .setSSLHostnameVerifier(new NoopHostnameVerifier())
                    .build();
        return client;
    }
    @Bean
    public SSLContext sslContext(){
        SSLContext sslContext = null;
        try {
            sslContext = new SSLContextBuilder()
                    .loadTrustMaterial(null, (x509Certificates, s) -> true).build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            logger.error(e.getLocalizedMessage());
            e.printStackTrace();
        }
        return sslContext;
    }
}
