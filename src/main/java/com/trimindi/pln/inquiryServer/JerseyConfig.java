package com.trimindi.pln.inquiryServer;

import com.trimindi.pln.inquiryServer.controllers.TransaksiControllers;
import com.trimindi.pln.inquiryServer.exeption.*;
import com.trimindi.pln.inquiryServer.filter.CORSFilter;
import com.trimindi.pln.inquiryServer.filter.SecureEndpoint;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;

/**
 * Created by PC on 08/08/2017.
 */
@Component
@ApplicationPath("/api/v1/")
public class JerseyConfig extends ResourceConfig {

    public JerseyConfig() {
        register(SecureEndpoint.class);
        register(CORSFilter.class);
        register(TransaksiControllers.class);
        register(BeanValConstrainViolationExceptionMapper.class);
        register(CustomBadRequestExeption.class);
        register(CustomExeption.class);
        register(CustomInvalidJsonParseExeption.class);
        register(CustomNotAllowedException.class);
        register(CustomNotFoundExeption.class);
        register(JacksonFeature.class);
    }
}
