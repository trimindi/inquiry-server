package com.trimindi.pln.inquiryServer;


import com.trimindi.pln.inquiryServer.response.postpaid.Rincian;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by PC on 6/7/2017.
 */
public class PostpaidHelper {

    public static String generatePeriode(List<Rincian> rincians){
        StringBuilder s = new StringBuilder();
        if(rincians.size() > 1){
            for (int i=0;i<rincians.size();i++) {
                if(i < rincians.size() - 1){
                    s.append(createPeriode(rincians.get(i).getBillPeriod().trim()) + " , ");
                }else{
                    s.append(createPeriode(rincians.get(i).getBillPeriod().trim()));
                }
            }
        }else{
            s.append(createPeriode(rincians.get(0).getBillPeriod()));
        }
        return s.toString();
    }
    public static String generateStandMeter(List<Rincian> rincians){
        StringBuilder s = new StringBuilder();
        if(rincians.size() > 1){
            s.append(new DecimalFormat("#").format(rincians.get(0).getPreviousMeter()) + " - " + new DecimalFormat("#").format(rincians.get(rincians.size() -1).getCurrentMeter()));
        }else{
            s.append(new DecimalFormat("#").format(rincians.get(0).getPreviousMeter()) + " - " + new DecimalFormat("#").format(rincians.get(0).getCurrentMeter()));
        }
        return s.toString();
    }

    public static String createPeriode(String period){

        period.trim();
        String tahun = period.substring(2,4);
        String bulan = period.substring(4,6);
        switch (bulan){
            case "01":
                return "JAN"+tahun;
            case "02":
                return "FEB"+tahun;
            case "03":
                return "MAR"+tahun;
            case "04":
                return "APR"+tahun;
            case "05":
                return "MEI"+tahun;
            case "06":
                return "JUN"+tahun;
            case "07":
                return "JUL"+tahun;
            case "08":
                return "AUG"+tahun;
            case "09":
                return "SEP"+tahun;
            case "10":
                return "OCT"+tahun;
            case "11":
                return "NOV"+tahun;
            case "12":
                return "DEC"+tahun;
        }
        return null;
    }
}
