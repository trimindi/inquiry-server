package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by PC on 6/12/2017.
 */
@Entity
@Table(name = "mst_biller")
public class Biller {

    @Id
    private String BILLER_ID;
    private String NAME;
    private String DESC;

    public String getBILLER_ID() {
        return BILLER_ID;
    }

    public Biller setBILLER_ID(String BILLER_ID) {
        this.BILLER_ID = BILLER_ID;
        return this;
    }

    public String getNAME() {
        return NAME;
    }

    public Biller setNAME(String NAME) {
        this.NAME = NAME;
        return this;
    }

    public String getDESC() {
        return DESC;
    }

    public Biller setDESC(String DESC) {
        this.DESC = DESC;
        return this;
    }
}
