package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by HP on 14/05/2017.
 */
@Entity
@Table(name = "mst_partner_pic")
@XmlRootElement
public class PartnerPIC {
    @Id
    private String ID;
    private String NAME;
    private String ADDRESS;
    private String PHONE;
    private String EMAIL;

    public PartnerPIC() {
    }

    public String getID() {
        return ID;
    }

    public PartnerPIC setID(String ID) {
        this.ID = ID;
        return this;
    }

    public String getNAME() {
        return NAME;
    }

    public PartnerPIC setNAME(String NAME) {
        this.NAME = NAME;
        return this;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public PartnerPIC setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
        return this;
    }

    public String getPHONE() {
        return PHONE;
    }

    public PartnerPIC setPHONE(String PHONE) {
        this.PHONE = PHONE;
        return this;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public PartnerPIC setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
        return this;
    }
}
