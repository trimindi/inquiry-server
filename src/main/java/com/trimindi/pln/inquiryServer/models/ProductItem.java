package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * Created by sx on 11/05/17.
 */

@Entity
@Table(name = "mst_product_item")
@XmlRootElement
public class ProductItem {

    @Column(name = "FK_PRODUCT_ID")
    private String product_id;
    @Column(name = "FK_BILLER")
    private String fk_biller;
    @Id
    private String DENOM;
    private String NAME;
    private String DESC;
    private double AMOUT;
    private double DEFAULT_FEE;
    private double FEE_MIN;
    private double FEE_MAX;
    private boolean CAN_UPDATE;
    private double ADMIN;
    @Column(name = "ACTIVE")
    private int ACTIVE;

    public boolean isACTIVE() {
        return (ACTIVE == 1);
    }

    public String getFk_biller() {
        return fk_biller;
    }

    public ProductItem setFk_biller(String fk_biller) {
        this.fk_biller = fk_biller;
        return this;
    }

    public int getACTIVE() {
        return ACTIVE;
    }

    @Override
    public String toString() {
        return "ProductItem{" +
                "product_id='" + product_id + '\'' +
                ", DENOM='" + DENOM + '\'' +
                ", NAME='" + NAME + '\'' +
                ", DESC='" + DESC + '\'' +
                ", AMOUT=" + AMOUT +
                ", DEFAULT_FEE=" + DEFAULT_FEE +
                ", FEE_MIN=" + FEE_MIN +
                ", FEE_MAX=" + FEE_MAX +
                ", CAN_UPDATE=" + CAN_UPDATE +
                ", ADMIN=" + ADMIN +
                ", ACTIVE=" + ACTIVE +
                '}';
    }

    public ProductItem setACTIVE(int ACTIVE) {
        this.ACTIVE = ACTIVE;
        return this;
    }

    public ProductItem() {
    }

    @XmlTransient
    public String getProduct_id() {
        return product_id;
    }

    public ProductItem setProduct_id(String product_id) {
        this.product_id = product_id;
        return this;
    }

    public String getDENOM() {
        return DENOM;
    }

    public ProductItem setDENOM(String DENOM) {
        this.DENOM = DENOM;
        return this;
    }

    public String getNAME() {
        return NAME;
    }

    public ProductItem setNAME(String NAME) {
        this.NAME = NAME;
        return this;
    }

    public String getDESC() {
        return DESC;
    }

    public ProductItem setDESC(String DESC) {
        this.DESC = DESC;
        return this;
    }

    public double getAMOUT() {
        return AMOUT;
    }

    public ProductItem setAMOUT(double AMOUT) {
        this.AMOUT = AMOUT;
        return this;
    }

    public double getDEFAULT_FEE() {
        return DEFAULT_FEE;
    }

    public ProductItem setDEFAULT_FEE(double DEFAULT_FEE) {
        this.DEFAULT_FEE = DEFAULT_FEE;
        return this;
    }

    @XmlTransient
    public double getFEE_MIN() {
        return FEE_MIN;
    }

    public ProductItem setFEE_MIN(double FEE_MIN) {
        this.FEE_MIN = FEE_MIN;
        return this;
    }

    @XmlTransient
    public double getFEE_MAX() {
        return FEE_MAX;
    }

    public ProductItem setFEE_MAX(double FEE_MAX) {
        this.FEE_MAX = FEE_MAX;
        return this;
    }

    public boolean isCAN_UPDATE() {
        return CAN_UPDATE;
    }

    public ProductItem setCAN_UPDATE(boolean CAN_UPDATE) {
        this.CAN_UPDATE = CAN_UPDATE;
        return this;
    }

    public double getADMIN() {
        return ADMIN;
    }

    public ProductItem setADMIN(double ADMIN) {
        this.ADMIN = ADMIN;
        return this;
    }
}
