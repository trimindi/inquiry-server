package com.trimindi.pln.inquiryServer.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * Created by HP on 14/05/2017.
 */

@Entity
@Table(name = "user_id")
@XmlRootElement
public class PartnerCredential implements Serializable{
    @Column(name = "FK_PARTNER_ID")
    private String partner_id;
    @Id
    @Column(name = "USERID")
    private String partner_uid;

    @Override
    public String toString() {
        return "PartnerCredential{" +
                "partner_id='" + partner_id + '\'' +
                ", partner_uid='" + partner_uid + '\'' +
                ", state=" + state +
                ", merchant_type='" + merchant_type + '\'' +
                ", back_link='" + back_link + '\'' +
                ", partner_password='" + partner_password + '\'' +
                ", partner_name='" + partner_name + '\'' +
                '}';
    }

    @Column(name = "STATE")
    private int state;

    @Column(name = "MERCHANT_TYPE")
    private String merchant_type;

    @Column(name = "BACK_LINK")
    private String back_link;

    @Column(name = "PASSWORD")
    private String partner_password;
    @Column(name = "NAME")
    private String partner_name;
    @Column(name = "IP_ADDRESS")
    private String ip_address;

    public PartnerCredential() {
    }

    public int getState() {
        return state;
    }

    public PartnerCredential setState(int state) {
        this.state = state;
        return this;
    }

    public String getMerchant_type() {
        return merchant_type;
    }

    public PartnerCredential setMerchant_type(String merchant_type) {
        this.merchant_type = merchant_type;
        return this;
    }

    public String getBack_link() {
        return back_link;
    }

    public PartnerCredential setBack_link(String back_link) {
        this.back_link = back_link;
        return this;
    }

    public String getPartner_id() {
        return partner_id;
    }

    public void setPartner_id(String partner_id) {
        this.partner_id = partner_id;
    }

    public String getPartner_uid() {
        return partner_uid;
    }

    public void setPartner_uid(String partner_uid) {
        this.partner_uid = partner_uid;
    }


    @XmlTransient
    public String getPartner_password() {
        return partner_password;
    }

    public void setPartner_password(String partner_password) {
        this.partner_password = partner_password;
    }

    public String getPartner_name() {
        return partner_name;
    }

    public void setPartner_name(String partner_name) {
        this.partner_name = partner_name;
    }

    public String getIp_address() {
        return ip_address;
    }

    public PartnerCredential setIp_address(String ip_address) {
        this.ip_address = ip_address;
        return this;
    }
}
