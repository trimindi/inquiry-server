package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 20/05/2017.
 */
@Entity
@Table(name = "trans_state")
public class TransaksiState {

    @Id
    private String ID;
    private String DESC;

    public String getID() {
        return ID;
    }

    public TransaksiState setID(String ID) {
        this.ID = ID;
        return this;
    }

    public String getDESC() {
        return DESC;
    }

    public TransaksiState setDESC(String DESC) {
        this.DESC = DESC;
        return this;
    }
}
