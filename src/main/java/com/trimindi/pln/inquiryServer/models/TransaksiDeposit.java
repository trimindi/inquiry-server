package com.trimindi.pln.inquiryServer.models;


import com.trimindi.pln.inquiryServer.TimestampAdapter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.sql.Timestamp;

/**
 * Created by HP on 22/05/2017.
 */
@Entity
@Table(name = "trans_deposit")
public class TransaksiDeposit {

    @Id
    private String NTRANS;
    private double MASUK;
    private double KELUAR;
    private double BALANCE;
    @XmlElement(name = "DATE", required = true,nillable=true)
    @XmlJavaTypeAdapter(TimestampAdapter.class)
    private Timestamp DATE;

    public TransaksiDeposit() {
        this.DATE = new Timestamp(System.currentTimeMillis());
    }

    public Timestamp getDATE() {
        return DATE;
    }

    public TransaksiDeposit setDATE(Timestamp DATE) {
        this.DATE = DATE;
        return this;
    }


    public String getNTRANS() {
        return NTRANS;
    }

    public TransaksiDeposit setNTRANS(String NTRANS) {
        this.NTRANS = NTRANS;
        return this;
    }

    public double getMASUK() {
        return MASUK;
    }

    public TransaksiDeposit setMASUK(double MASUK) {
        this.MASUK = MASUK;
        return this;
    }

    public double getKELUAR() {
        return KELUAR;
    }

    public TransaksiDeposit setKELUAR(double KELUAR) {
        this.KELUAR = KELUAR;
        return this;
    }

    public double getBALANCE() {
        return BALANCE;
    }

    public TransaksiDeposit setBALANCE(double BALANCE) {
        this.BALANCE = BALANCE;
        return this;
    }
}
