package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 20/05/2017.
 */
@Entity
@Table(name = "mst_merchant")
public class Merchant {

    @Id
    private String ID;
    private String NAME;
    private String DESC;

    public String getDESC() {
        return DESC;
    }

    public Merchant setDESC(String DESC) {
        this.DESC = DESC;
        return this;
    }

    public String getID() {
        return ID;
    }

    public Merchant setID(String ID) {
        this.ID = ID;
        return this;
    }

    public String getNAME() {
        return NAME;
    }

    public Merchant setNAME(String NAME) {
        this.NAME = NAME;
        return this;
    }
}
