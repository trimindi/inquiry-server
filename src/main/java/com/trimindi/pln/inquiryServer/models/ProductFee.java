package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 19/05/2017.
 */
@Entity
@Table(name = "mst_product_price")
public class ProductFee {
    @Id
    private int ID;
    private String PARTNER_ID;
    private String PRODUCT_ITEM;
    private double FEE_MAX;
    private double FEE_MIN;
    private double FEE;
    private boolean CAN_UPDATE;
    public ProductFee() {

    }

    public int getID() {
        return ID;
    }

    public ProductFee setID(int ID) {
        this.ID = ID;
        return this;
    }

    public String getPARTNER_ID() {
        return PARTNER_ID;
    }

    public ProductFee setPARTNER_ID(String PARTNER_ID) {
        this.PARTNER_ID = PARTNER_ID;
        return this;
    }

    public String getPRODUCT_ITEM() {
        return PRODUCT_ITEM;
    }

    public ProductFee setPRODUCT_ITEM(String PRODUCT_ITEM) {
        this.PRODUCT_ITEM = PRODUCT_ITEM;
        return this;
    }

    public double getFEE_MAX() {
        return FEE_MAX;
    }

    public ProductFee setFEE_MAX(double FEE_MAX) {
        this.FEE_MAX = FEE_MAX;
        return this;
    }

    public double getFEE_MIN() {
        return FEE_MIN;
    }

    public ProductFee setFEE_MIN(double FEE_MIN) {
        this.FEE_MIN = FEE_MIN;
        return this;
    }

    public double getFEE() {
        return FEE;
    }

    public ProductFee setFEE(double FEE) {
        this.FEE = FEE;
        return this;
    }

    public boolean isCAN_UPDATE() {
        return CAN_UPDATE;
    }

    public ProductFee setCAN_UPDATE(boolean CAN_UPDATE) {
        this.CAN_UPDATE = CAN_UPDATE;
        return this;
    }
}
