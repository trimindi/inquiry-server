package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 14/05/2017.
 */
@Entity
//@XmlRootElement
@Table(name = "mst_partner")
public class Partner {

    @Id
    private String PARTNER_ID;
    private String NAME;
    private String PHONE;
    private String ADDRESS;
    private String EMAIL;

    @Column(name = "FK_PIC_ID")
    private String partner_pic;
    public Partner() {
    }

    public Partner(String PARTNER_ID, String NAME, String PHONE, String ADDRESS, String EMAIL, String partner_pic) {
        this.PARTNER_ID = PARTNER_ID;
        this.NAME = NAME;
        this.PHONE = PHONE;
        this.ADDRESS = ADDRESS;
        this.EMAIL = EMAIL;
        this.partner_pic = partner_pic;
    }

    public String getPARTNER_ID() {
        return PARTNER_ID;
    }

    public void setPARTNER_ID(String PARTNER_ID) {
        this.PARTNER_ID = PARTNER_ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getPHONE() {
        return PHONE;
    }

    public void setPHONE(String PHONE) {
        this.PHONE = PHONE;
    }

    public String getADDRESS() {
        return ADDRESS;
    }

    public void setADDRESS(String ADDRESS) {
        this.ADDRESS = ADDRESS;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getPartner_pic() {
        return partner_pic;
    }

    public void setPartner_pic(String partner_pic) {
        this.partner_pic = partner_pic;
    }
}
