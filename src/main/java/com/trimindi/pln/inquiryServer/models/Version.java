package com.trimindi.pln.inquiryServer.models;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by HP on 21/05/2017.
 */
@Entity
@Table(name = "version")
@XmlRootElement
public class Version {
    @Id
    @Column(name = "MT_ID")
    private String id;
    @Column(name = "VERSION")
    private String version;
    @Column(name = "LINK")
    private String link;

    public Version() {
    }

    public String getId() {
        return id;
    }

    public Version setId(String id) {
        this.id = id;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public Version setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getLink() {
        return link;
    }

    public Version setLink(String link) {
        this.link = link;
        return this;
    }
}
