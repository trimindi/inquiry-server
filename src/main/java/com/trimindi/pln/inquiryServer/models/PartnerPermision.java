package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by PC on 21/07/2017.
 */

@Entity
@Table(name = "user_permision")
public class PartnerPermision {

    @Id
    @Column(name = "FK_USERID")
    private String partner_uid;

    @Column(name = "PLN")
    private Boolean pln;
    @Column(name = "TELKOM")
    private Boolean telkom;
    @Column(name = "PULSA")
    private Boolean pulsa;
    @Column(name = "PDAM")
    private Boolean pdam;
    @Column(name = "BIGBILL")
    private Boolean bigbill;

    public PartnerPermision() {
    }

    public String getPartner_uid() {
        return partner_uid;
    }

    public PartnerPermision setPartner_uid(String partner_uid) {
        this.partner_uid = partner_uid;
        return this;
    }

    public Boolean getPln() {
        return pln;
    }

    public PartnerPermision setPln(Boolean pln) {
        this.pln = pln;
        return this;
    }

    public Boolean getTelkom() {
        return telkom;
    }

    public PartnerPermision setTelkom(Boolean telkom) {
        this.telkom = telkom;
        return this;
    }

    public Boolean getPulsa() {
        return pulsa;
    }

    public PartnerPermision setPulsa(Boolean pulsa) {
        this.pulsa = pulsa;
        return this;
    }

    public Boolean getPdam() {
        return pdam;
    }

    public PartnerPermision setPdam(Boolean pdam) {
        this.pdam = pdam;
        return this;
    }

    public Boolean getBigbill() {
        return bigbill;
    }

    public PartnerPermision setBigbill(Boolean bigbill) {
        this.bigbill = bigbill;
        return this;
    }
}
