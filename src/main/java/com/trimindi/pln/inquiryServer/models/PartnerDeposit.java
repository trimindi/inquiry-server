package com.trimindi.pln.inquiryServer.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by HP on 19/05/2017.
 */

@Entity
@Table(name = "mst_deposit")
public class PartnerDeposit {

    @Id
    private String partner_id;
    private double MASUK;
    private double KELUAR;
    private double BALANCE;

    public PartnerDeposit() {
    }

    public String getPartner_id() {
        return partner_id;
    }

    public PartnerDeposit setPartner_id(String partner_id) {
        this.partner_id = partner_id;
        return this;
    }

    public double getMASUK() {
        return MASUK;
    }

    public PartnerDeposit setMASUK(double MASUK) {
        this.MASUK = MASUK;
        return this;
    }

    public double getKELUAR() {
        return KELUAR;
    }

    public PartnerDeposit setKELUAR(double KELUAR) {
        this.KELUAR = KELUAR;
        return this;
    }

    public double getBALANCE() {
        return BALANCE;
    }

    public PartnerDeposit setBALANCE(double BALANCE) {
        this.BALANCE = BALANCE;
        return this;
    }
}
