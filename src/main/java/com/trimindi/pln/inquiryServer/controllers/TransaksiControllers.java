package com.trimindi.pln.inquiryServer.controllers;

import com.solab.iso8583.IsoMessage;
import com.trimindi.pln.inquiryServer.AppProperties;
import com.trimindi.pln.inquiryServer.PostpaidHelper;
import com.trimindi.pln.inquiryServer.client.IsoMessageListener;
import com.trimindi.pln.inquiryServer.client.client.Iso8583Client;
import com.trimindi.pln.inquiryServer.models.PartnerCredential;
import com.trimindi.pln.inquiryServer.models.ProductItem;
import com.trimindi.pln.inquiryServer.response.postpaid.Rincian;
import com.trimindi.pln.inquiryServer.response.prepaid.Inquiry;
import com.trimindi.pln.inquiryServer.response.prepaid.InquiryResponse;
import com.trimindi.pln.inquiryServer.services.product.ProductItemService;
import com.trimindi.pln.inquiryServer.utils.constanta.Constanta;
import com.trimindi.pln.inquiryServer.utils.constanta.ResponseCode;
import com.trimindi.pln.inquiryServer.utils.generator.MessageGenerator;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;
import com.trimindi.pln.inquiryServer.utils.iso.parsing.SDE;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorNonTagList;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorPostPaid;
import com.trimindi.pln.inquiryServer.utils.rules.response.ResponseRulesGeneratorPrePaid;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Created by PC on 31/08/2017.
 */
@Path("/")
@Component
@Scope(value = "request")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class TransaksiControllers {
    public static final Logger logger = LoggerFactory.getLogger(TransaksiControllers.class);
    private static final Map<String, ResponseCode> failedCode;
    private static final Map<String, ResponseCode> responseCode;

    static {
        Map<String, ResponseCode> aMap = new HashMap<>();
        try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("response.properties")) {
            final Properties properties = new Properties();
            properties.load(stream);
            properties.forEach((key, value) -> {
                aMap.put(String.valueOf(key), new ResponseCode((String) key, (String) value));
            });
        } catch (IOException | NumberFormatException e) {
            throw new IllegalStateException("Unable to Response Code dictionary", e);
        }
        responseCode = Collections.unmodifiableMap(aMap);
        Map<String, ResponseCode> aMap2 = new HashMap<>();
        aMap2.put("0005", new ResponseCode("0005", responseCode.get("0005").getMessage()));
        aMap2.put("0063", new ResponseCode("0063", responseCode.get("0063").getMessage()));
        aMap2.put("0068", new ResponseCode("0068", responseCode.get("0068").getMessage()));
        failedCode = Collections.unmodifiableMap(aMap2);
    }

    private final
    MessageGenerator messageGenerator;
    private final
    ProductItemService productItemService;
    private final Iso8583Client<IsoMessage> isoMessageIso8583Client;
    private final AppProperties config;
    private IsoMessage inquiryRequest;
    private Map<String, String> params = new HashMap<>();
    private ProductItem productItem;
    private String dueDate;

    @Autowired
    public TransaksiControllers(MessageGenerator messageGenerator, ProductItemService productItemService, Iso8583Client<IsoMessage> isoMessageIso8583Client, AppProperties config) {
        this.messageGenerator = messageGenerator;
        this.productItemService = productItemService;
        this.isoMessageIso8583Client = isoMessageIso8583Client;
        this.config = config;
    }

    @POST
    public void inquiry(@Suspended AsyncResponse asyncResponse,
                        @Context ContainerRequestContext security,
                        @Valid Request req) {
        PartnerCredential p = (PartnerCredential) security.getProperty(Constanta.PRINCIPAL);
        params.put(Constanta.MACHINE_TYPE, p.getMerchant_type());
        params.put(Constanta.MSSIDN, req.MSSIDN);
        params.put(Constanta.AREA, req.AREA);
        params.put(Constanta.TID, req.TID);
        params.put(Constanta.IP_ADDRESS, String.valueOf(security.getProperty(Constanta.IP_ADDRESS)));
        params.put(Constanta.MAC, String.valueOf(security.getProperty(Constanta.MAC)));
        params.put(Constanta.LONGITUDE, String.valueOf(security.getProperty(Constanta.LONGITUDE)));
        params.put(Constanta.LATITUDE, String.valueOf(security.getProperty(Constanta.LATITUDE)));
        params.put(Constanta.DENOM, req.PRODUCT);
        params.put(Constanta.BACK_LINK, req.BACK_LINK);
        params.put(Constanta.PID, req.PID);
        asyncResponse.setTimeoutHandler(as -> {
            as.resume(Response.status(200)
                    .entity(ResponseCode.SERVER_TIMEOUT
                            .setProduct(req.PRODUCT)
                            .setMssidn(req.MSSIDN))
                    .build());
        });
        asyncResponse.setTimeout(60, TimeUnit.SECONDS);
        switch (req.ACTION) {
            case "PLN.INQUIRY.INSTANSI":
                productItem = productItemService.findById(req.PRODUCT);
                if (productItem == null) {
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_UNKNOW_DENOM).build());
                    return;
                }
                if (!productItem.getFk_biller().equalsIgnoreCase("001")) {
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PARAMETER_INVALID_PRODUCT_ACTION).build());
                    return;
                }
                if (!productItem.isACTIVE()) {
                    asyncResponse.resume(Response.status(200).entity(ResponseCode.PRODUCT_UNDER_MAINTANCE).build());
                    return;
                }
                switch (productItem.getProduct_id()) {
                    case "99501":
                        inquiryRequest = messageGenerator.inquiryPostpaid(req.MSSIDN, params.get(Constanta.MACHINE_TYPE), req.TID, req.PID);
                        break;
                    case "99502":
                        inquiryRequest = messageGenerator.inquiryPrepaid(params.get(Constanta.MACHINE_TYPE), req.MSSIDN, req.TID, req.PID);
                        break;
                    case "99504":
                        inquiryRequest = messageGenerator.inquiryNontaglist(req.MSSIDN, params.get(Constanta.MACHINE_TYPE), req.TID, req.PID);
                        break;
                }
                break;
            default:
                asyncResponse.resume(Response.status(200).entity(ResponseCode.UNKNOW_ACTION.setProduct(req.PRODUCT).setMssidn(req.MSSIDN)).build());
                break;
        }
        isoMessageIso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
            @Override
            public boolean applies(IsoMessage isoMessage) {
                return isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(2).equals(config.PAN_PREPAID) &&
                        isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
            }

            @Override
            public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                if (isoMessage.getObjectValue(39).equals("0000")) {
                    List<Rules> bit48 = new SDE.Builder()
                            .setPayload(isoMessage.getObjectValue(48))
                            .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(48, true))
                            .generate();
                    List<Rules> bit62 = new SDE.Builder()
                            .setPayload(isoMessage.getObjectValue(62))
                            .setRules(ResponseRulesGeneratorPrePaid.prePaidInquiryResponse(62, true))
                            .generate();
                    bit48.addAll(bit62);
                    Inquiry response = new Inquiry(bit48, true);
                    /**
                     * MT = MACHINE_TYPE
                     * PC = PRODUDUCT CODE
                     * PRODUCT = PRODUCT
                     */
                    response.setTagihan(0);
                    InquiryResponse baseResponse = new InquiryResponse();
                    baseResponse.setFee(0);
                    baseResponse.setSaldo(0);
                    baseResponse.setTagihan(productItem.getAMOUT());
                    baseResponse.setTotalBayar(productItem.getAMOUT() + productItem.getADMIN());
                    baseResponse.setTotalFee(0);
                    baseResponse.setData(response);
                    asyncResponse.resume(Response.status(200).entity(baseResponse).build());
                } else {
                    asyncResponse.resume(Response.status(200).entity(responseCode.get(isoMessage.getObjectValue(39).toString()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN)).build());
                }
                return false;
            }
        });

        isoMessageIso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
            @Override
            public boolean applies(IsoMessage isoMessage) {
                return isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(2).equals(config.PAN_NONTAGLIST) &&
                        isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
            }

            @Override
            public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                if (isoMessage.getObjectValue(39).equals("0000")) {
                    boolean status = true;
                    List<Rules> bit48 = new SDE.Builder()
                            .setPayload(isoMessage.getObjectValue(48))
                            .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(48, status))
                            .generate();
                    List<Rules> bit62 = new SDE.Builder()
                            .setPayload(isoMessage.getObjectValue(62))
                            .setRules(ResponseRulesGeneratorNonTagList.nonTagListInquiryResponse(62, status))
                            .generate();
                    bit48.addAll(bit62);
                    com.trimindi.pln.inquiryServer.response.nontaglist.Inquiry inquiryResponse = new com.trimindi.pln.inquiryServer.response.nontaglist.Inquiry(bit48, true);
                    com.trimindi.pln.inquiryServer.response.nontaglist.InquiryResponse baseResponse = new com.trimindi.pln.inquiryServer.response.nontaglist.InquiryResponse();
                    baseResponse.setData(inquiryResponse);
                    baseResponse.setFee(0);
                    baseResponse.setSaldo(0);
                    baseResponse.setTotalFee(0);
                    baseResponse.setTagihan(inquiryResponse.getTagihan());
                    baseResponse.setTotalBayar(inquiryResponse.getTagihan() + inquiryResponse.getAdmin());

                    asyncResponse.resume(
                            Response.status(200)
                                    .entity(baseResponse)
                                    .build()
                    );
                } else {
                    asyncResponse.resume(
                            Response.status(200)
                                    .entity(responseCode.get(isoMessage.getObjectValue(39).toString()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN))
                                    .build()
                    );
                }
                return false;
            }
        });

        isoMessageIso8583Client.addMessageListener(new IsoMessageListener<IsoMessage>() {
            @Override
            public boolean applies(IsoMessage isoMessage) {
                return isoMessage.getType() == 0x2110 && isoMessage.getObjectValue(2).equals(config.PAN_POSTPAID) &&
                        isoMessage.getObjectValue(11).equals(inquiryRequest.getObjectValue(11));
            }

            @Override
            public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
                try {
                    if (isoMessage.getObjectValue(39).equals("0000")) {
                        boolean status = true;
                        String inquiryRes = isoMessage.getObjectValue(48);
                        List<Rules> bit48 = new SDE.Builder()
                                .setPayload(inquiryRes)
                                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48, status))
                                .generate();
                        int legtht = new SDE.Builder()
                                .setPayload(inquiryRes)
                                .setRules(ResponseRulesGeneratorPostPaid.postPaidInquiryResponse(48, status))
                                .calculate();

                        String rincian = inquiryRes.substring(legtht, inquiryRes.length());
                        com.trimindi.pln.inquiryServer.response.postpaid.Inquiry inquiryResponse = new com.trimindi.pln.inquiryServer.response.postpaid.Inquiry(bit48, true);
                        List<Rincian> rincians = new ArrayList<>();
                        int start = 0;
                        int leghtRincian = 115;
                        double total = 0;
                        double denda = 0;
                        for (int i = 0; i < inquiryResponse.getBillStatus(); i++) {
                            String parsRincian = rincian.substring(start, start + leghtRincian);
                            List<Rules> rc = new SDE.Builder().setPayload(parsRincian).setRules(ResponseRulesGeneratorPostPaid.rulesRincian()).generate();
                            Rincian r = new Rincian(rc);
                            rincians.add(r);
                            dueDate = r.getDueDate();
                            total += r.getTotalElectricityBill();
                            denda += r.getPenaltyFee();
                            start += leghtRincian;

                        }
                        total += denda;
                        inquiryResponse.setRincian(rincians);
                        inquiryResponse.setPeriod(PostpaidHelper.generatePeriode(rincians));
                        inquiryResponse.setMeter(PostpaidHelper.generateStandMeter(rincians));
                        inquiryResponse.setTagihan(total - denda);
                        inquiryResponse.setDenda(denda);
                        inquiryResponse.setDuedate(dueDate);
                        com.trimindi.pln.inquiryServer.response.postpaid.InquiryResponse baseResponse = new com.trimindi.pln.inquiryServer.response.postpaid.InquiryResponse();
                        baseResponse.setData(inquiryResponse);
                        baseResponse.setFee(0);
                        baseResponse.setSaldo(0);
                        baseResponse.setTotalFee(0);
                        baseResponse.setTagihan(total - denda);
                        baseResponse.setTotalBayar(total + inquiryResponse.getAdmin());
                        asyncResponse.resume(
                                Response.status(200)
                                        .entity(baseResponse)
                                        .build()
                        );
                    } else {
                        asyncResponse.resume(
                                Response.status(200)
                                        .entity(responseCode.get(isoMessage.getObjectValue(39).toString()).setProduct(req.PRODUCT).setMssidn(req.MSSIDN))
                                        .build()
                        );
                    }
                } catch (Exception e) {
                    logger.error("Exception thrown {}", e.getMessage());
                }
                return false;
            }
        });
        if (inquiryRequest != null) {
            if (!isoMessageIso8583Client.isConnected()) {
                isoMessageIso8583Client.connectAsync();
            }
            isoMessageIso8583Client.sendAsync(inquiryRequest);
        }
    }
}
