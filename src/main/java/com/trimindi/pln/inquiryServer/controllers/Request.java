package com.trimindi.pln.inquiryServer.controllers;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by PC on 6/17/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Request {
    @NotNull(message = "PRODUCT REQUIRED")
    public String PRODUCT;
    @NotNull(message = "ACTION REQUIRED")
    public String ACTION;
    @NotNull(message = "MSSIDN REQUIRED")
    public String MSSIDN;
    public String NTRANS;
    public String AREA;
    public String BACK_LINK;
    @NotNull
    @Size(min = 16,max = 16,message = "TERMINAL ID REQUIRED")
    public String TID;
    @NotNull
    @Size(min = 7,max = 7,message = "PARTNER ID REQUIRED")
    public String PID;

    @Override
    public String toString() {
        return "Request{" +
                "PRODUCT='" + PRODUCT + '\'' +
                ", ACTION='" + ACTION + '\'' +
                ", MSSIDN='" + MSSIDN + '\'' +
                ", TID='" + TID + '\'' +
                ", PID='" + PID + '\'' +
                '}';
    }
}
