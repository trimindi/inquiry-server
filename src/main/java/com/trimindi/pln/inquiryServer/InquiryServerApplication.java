package com.trimindi.pln.inquiryServer;

import com.solab.iso8583.IsoMessage;
import com.trimindi.pln.inquiryServer.client.IsoMessageListener;
import com.trimindi.pln.inquiryServer.client.client.Iso8583Client;
import com.trimindi.pln.inquiryServer.utils.constanta.ResponseCode;
import com.trimindi.pln.inquiryServer.utils.generator.MessageGenerator;
import io.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@SpringBootApplication
public class InquiryServerApplication {
	public static final Map<String, ResponseCode> responseCode;

	static {
		Map<String, ResponseCode> aMap = new HashMap<>();
		try (InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream("response.properties")) {
			final Properties properties = new Properties();
			properties.load(stream);
			properties.forEach((key, value) -> {
				aMap.put(String.valueOf(key),new ResponseCode((String) key,(String) value));
			});
		} catch (IOException | NumberFormatException e) {
			throw new IllegalStateException("Unable to Response Code dictionary", e);
		}

		responseCode = Collections.unmodifiableMap(aMap);
	}

	Logger logger = LoggerFactory.getLogger(InquiryServerApplication.class);
	@Value("${iso8583.connection.host}")
	private String host;
	@Value("${iso8583.connection.port}")
	private int port;

	public static void main(String[] args) {
		SpringApplication.run(InquiryServerApplication.class, args);
	}

	@Bean
	CommandLineRunner commandLineRunner(Iso8583Client<IsoMessage> client, MessageGenerator messageGenerator){
		return args -> {
			client.init();
			client.addMessageListener(new IsoMessageListener<IsoMessage>() {
				@Override
				public boolean applies(IsoMessage isoMessage) {
					return isoMessage.getType() == 0x2810;
				}

				@Override
				public boolean onMessage(ChannelHandlerContext ctx, IsoMessage isoMessage) {
					if (isoMessage.getObjectValue(39).equals("0000")) {
						switch (isoMessage.getObjectValue(40).toString()) {
							case "001":
								logger.error("SUCCESS SIGN ON KE SERVER");
								break;
							case "002":
								logger.error("SUCCESS SIGN OFF KE SERVER");
								break;
							case "003":
								logger.error("CONNECTION ESTABILISED");
								break;
						}
					} else {
						logger.error("FAILED CONNECT TO SERVER " + responseCode.get(isoMessage.getObjectValue(39).toString()).getMessage());
					}
					return false;
				}
			});
			client.getConfiguration().replyOnError();
			client.connectAsync();
			if (client.isConnected()) {
				IsoMessage message = messageGenerator.signON();
				client.sendAsync(message);
			}

		};
	}
}
