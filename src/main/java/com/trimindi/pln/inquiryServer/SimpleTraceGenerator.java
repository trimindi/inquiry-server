package com.trimindi.pln.inquiryServer;

import com.solab.iso8583.IsoMessage;
import com.trimindi.pln.inquiryServer.client.client.Iso8583Client;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by PC on 10/08/2017.
 */
@Component
public class SimpleTraceGenerator {
    private static final Logger logger = LoggerFactory.getLogger(SimpleTraceGenerator.class);
    private volatile int value = 1;
    private String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    private BufferedReader bufferedReader;

    @Value("${pln.stan}")
    private String uniqStan;

    @Autowired
    private Iso8583Client<IsoMessage> isoMessageIso8583Client;

    public SimpleTraceGenerator() {

    }

    @PostConstruct
    public void init(){
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            bufferedReader = new BufferedReader(new FileReader("stan.resume"));
            String[] val = bufferedReader.readLine().split(",");
            if(val[0].equalsIgnoreCase(date)){
                value = Integer.parseInt(val[1]);
            }else{
                value = 1;
            }
        } catch (IOException e) {
            logger.info("CANT FIND FILE START NEW STAN NUMBER");
            create();
        }
    }

    public int getLastTrace() {
        return this.value;
    }
    @PreDestroy
    public void destory(){
        logger.info("SAVING CURENT STAN NUMBER");
        create();
        if (isoMessageIso8583Client.isConnected()) {
            try {
                isoMessageIso8583Client.disconnect();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void create(){
        try {
            date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
            FileWriter fileWriter = new FileWriter("stan.resume");
            fileWriter.write(date + "," + String.valueOf(value));
            fileWriter.flush();
            fileWriter.close();
            logger.info("SUCCESS SAVING STAN NUMBER");
        } catch (IOException e) {
            logger.error("FAILED SAVING STAN NUMBER");
            logger.error(e.getLocalizedMessage());
            e.printStackTrace();
        }
    }

    public synchronized String nextTrace() {
        ++this.value;
        if(this.value > 999999999) {
            this.value = 1;
        }

        return uniqStan + StringUtils.leftPad(String.valueOf(this.value),9,"0");
    }
}
