package com.trimindi.pln.inquiryServer;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by PC on 6/5/2017.
 */
public class TimestampAdapter extends XmlAdapter<String,Timestamp> {
    @Override
    public Timestamp unmarshal(String v) throws Exception {
        return null;
    }

    @Override
    public String marshal(Timestamp v) throws Exception {
        if(v != null){
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(v);
        }else{
            return null;
        }
    }
}
