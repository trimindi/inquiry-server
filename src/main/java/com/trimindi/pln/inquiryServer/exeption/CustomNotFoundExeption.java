package com.trimindi.pln.inquiryServer.exeption;



import com.trimindi.pln.inquiryServer.utils.constanta.ResponseCode;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by PC on 6/17/2017.
 */
@Provider
public class CustomNotFoundExeption implements ExceptionMapper<NotFoundException> {

    @Override
    public Response toResponse(NotFoundException e) {
        e.printStackTrace();
        return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build();
    }
}
