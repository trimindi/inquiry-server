package com.trimindi.pln.inquiryServer.exeption;


import com.trimindi.pln.inquiryServer.utils.constanta.ResponseCode;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by PC on 08/08/2017.
 */
@Provider
public class BeanValConstrainViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {

    @Override
    public Response toResponse(ConstraintViolationException e) {
        ConstraintViolation cv = (ConstraintViolation) e.getConstraintViolations().toArray()[0];
        return Response.status(200)
                .entity(ResponseCode.INVALID_BODY_REQUEST_FORMAT.setMessage(cv.getMessage()))
                .build();
    }
}