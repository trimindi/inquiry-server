package com.trimindi.pln.inquiryServer.exeption;

import com.fasterxml.jackson.core.JsonParseException;
import com.trimindi.pln.inquiryServer.utils.constanta.ResponseCode;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by PC on 6/17/2017.
 */
@Provider
public class CustomInvalidJsonParseExeption implements ExceptionMapper<JsonParseException> {

    @Override
    public Response toResponse(JsonParseException e) {
        e.printStackTrace();
        return Response.status(200).entity(ResponseCode.INVALID_BODY_REQUEST_FORMAT).build();
    }
}
