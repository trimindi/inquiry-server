package com.trimindi.pln.inquiryServer.exeption;



import com.trimindi.pln.inquiryServer.utils.constanta.ResponseCode;

import javax.ws.rs.NotAllowedException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by PC on 6/12/2017.
 */
@Provider
public class CustomNotAllowedException implements ExceptionMapper<NotAllowedException> {

    @Override
    public Response toResponse(NotAllowedException e) {
        e.printStackTrace();
        return Response.status(200).entity(ResponseCode.SERVER_UNAVAILABLE).build();
    }
}
