package com.trimindi.pln.inquiryServer.response.nontaglist;


import com.trimindi.pln.inquiryServer.utils.generator.BaseHelper;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */

@XmlRootElement
public class Payment extends BaseHelper implements Serializable {
    private String SwitcherID;
    private String RegistrationNumber;
    private String AreaCode;
    private String TransactionCode;
    private String TransactionName;
    private String RegistrationDate;
    private String ExpirationDate;
    private String SubscriberID;
    private String SubscriberName;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String ServiceUnit;
    private String ServiceUnitAddress;
    private String ServiceUnitPhone;
    private int TotalTransactionAmountMinorUnit;
    private double tagihan;
    private int PLNBILLMinorUnit;
    private double PLNBILLValue;
    private int AdministrationChargeMinorUnit;
    private double admin;
    /**
     * 62
     */
    private String MutationNumber;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private String BukopinReference;

    /**
     * 62
     */
    private String Billcomponenttype;
    private int BillComponentMinorUnit;
    private double BillComponentValueAmount;

    /**
     * 63
     */

    private String message;

    public Payment() {
    }

    public Payment(List<Rules> rules) {
        this.SwitcherID = (String) rules.get(0).getResults();
        this.RegistrationNumber = (String) rules.get(1).getResults();
        this.AreaCode = (String) rules.get(2).getResults();
        this.TransactionCode = (String) rules.get(3).getResults();
        this.TransactionName = (String) rules.get(4).getResults();
        this.RegistrationDate = (String) rules.get(5).getResults();
        this.ExpirationDate = (String) rules.get(6).getResults();
        this.SubscriberID = (String) rules.get(7).getResults();
        this.SubscriberName = (String) rules.get(8).getResults();
        this.PLNReferenceNumber = (String) rules.get(9).getResults();
        this.BukopinReferenceNumber = (String) rules.get(10).getResults();
        this.ServiceUnit = (String) rules.get(11).getResults();
        this.ServiceUnitAddress = (String) rules.get(12).getResults();
        this.ServiceUnitPhone = (String) rules.get(13).getResults();
        this.TotalTransactionAmountMinorUnit = Integer.parseInt((String) rules.get(14).getResults());
        this.tagihan = numberMinorUnit((String) rules.get(15).getResults(), this.TotalTransactionAmountMinorUnit);
        this.PLNBILLMinorUnit = Integer.parseInt((String) rules.get(16).getResults());
        this.PLNBILLValue = numberMinorUnit((String) rules.get(17).getResults(), this.PLNBILLMinorUnit);
        this.AdministrationChargeMinorUnit = Integer.parseInt((String) rules.get(18).getResults());
        this.admin = numberMinorUnit((String) rules.get(19).getResults(), this.AdministrationChargeMinorUnit);
        this.MutationNumber = (String) rules.get(20).getResults();
        this.SubscriberSegmentation = (String) rules.get(21).getResults();
        this.PowerConsumingCategory = Integer.parseInt((String) rules.get(22).getResults());
        this.BukopinReference = (String) rules.get(23).getResults();
        this.Billcomponenttype = (String) rules.get(24).getResults();
        this.BillComponentMinorUnit = Integer.parseInt((String) rules.get(25).getResults());
        this.BillComponentValueAmount = numberMinorUnit((String) rules.get(26).getResults(), this.BillComponentMinorUnit);
        this.message = (String) rules.get(27).getResults();
    }

    @Override
    public String toString() {
        return "Payment{" +
                "SwitcherID='" + SwitcherID + '\'' +
                ", RegistrationNumber='" + RegistrationNumber + '\'' +
                ", AreaCode='" + AreaCode + '\'' +
                ", TransactionCode='" + TransactionCode + '\'' +
                ", TransactionName='" + TransactionName + '\'' +
                ", RegistrationDate='" + RegistrationDate + '\'' +
                ", ExpirationDate='" + ExpirationDate + '\'' +
                ", SubscriberID='" + SubscriberID + '\'' +
                ", SubscriberName='" + SubscriberName + '\'' +
                ", PLNReferenceNumber='" + PLNReferenceNumber + '\'' +
                ", BukopinReferenceNumber='" + BukopinReferenceNumber + '\'' +
                ", ServiceUnit='" + ServiceUnit + '\'' +
                ", ServiceUnitAddress='" + ServiceUnitAddress + '\'' +
                ", ServiceUnitPhone='" + ServiceUnitPhone + '\'' +
                ", TotalTransactionAmountMinorUnit=" + TotalTransactionAmountMinorUnit +
                ", tagihan=" + tagihan +
                ", PLNBILLMinorUnit=" + PLNBILLMinorUnit +
                ", PLNBILLValue=" + PLNBILLValue +
                ", AdministrationChargeMinorUnit=" + AdministrationChargeMinorUnit +
                ", admin=" + admin +
                ", MutationNumber='" + MutationNumber + '\'' +
                ", SubscriberSegmentation='" + SubscriberSegmentation + '\'' +
                ", PowerConsumingCategory=" + PowerConsumingCategory +
                ", BukopinReference='" + BukopinReference + '\'' +
                ", Billcomponenttype='" + Billcomponenttype + '\'' +
                ", BillComponentMinorUnit=" + BillComponentMinorUnit +
                ", BillComponentValueAmount=" + BillComponentValueAmount +
                ", message='" + message + '\'' +
                '}';
    }

    @XmlTransient
    public String getSwitcherID() {
        return SwitcherID;
    }

    public Payment setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
        return this;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public Payment setRegistrationNumber(String registrationNumber) {
        RegistrationNumber = registrationNumber;
        return this;
    }

    @XmlTransient
    public String getAreaCode() {
        return AreaCode;
    }

    public Payment setAreaCode(String areaCode) {
        AreaCode = areaCode;
        return this;
    }

    @XmlTransient
    public String getTransactionCode() {
        return TransactionCode;
    }

    public Payment setTransactionCode(String transactionCode) {
        TransactionCode = transactionCode;
        return this;
    }

    public String getTransactionName() {
        return TransactionName;
    }

    public Payment setTransactionName(String transactionName) {
        TransactionName = transactionName;
        return this;
    }

    public String getRegistrationDate() {
        return RegistrationDate;
    }

    public Payment setRegistrationDate(String registrationDate) {
        RegistrationDate = registrationDate;
        return this;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public Payment setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
        return this;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public Payment setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
        return this;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public Payment setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
        return this;
    }

    @XmlTransient
    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public Payment setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
        return this;
    }

    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public Payment setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
        return this;
    }

    @XmlTransient
    public String getServiceUnit() {
        return ServiceUnit;
    }

    public Payment setServiceUnit(String serviceUnit) {
        ServiceUnit = serviceUnit;
        return this;
    }


    public String getServiceUnitAddress() {
        return ServiceUnitAddress;
    }

    public Payment setServiceUnitAddress(String serviceUnitAddress) {
        ServiceUnitAddress = serviceUnitAddress;
        return this;
    }

    public String getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public Payment setServiceUnitPhone(String serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
        return this;
    }

    @XmlTransient
    public int getTotalTransactionAmountMinorUnit() {
        return TotalTransactionAmountMinorUnit;
    }

    public Payment setTotalTransactionAmountMinorUnit(int totalTransactionAmountMinorUnit) {
        TotalTransactionAmountMinorUnit = totalTransactionAmountMinorUnit;
        return this;
    }


    public double getTagihan() {
        return tagihan;
    }

    public Payment setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    @XmlTransient
    public int getPLNBILLMinorUnit() {
        return PLNBILLMinorUnit;
    }

    public Payment setPLNBILLMinorUnit(int PLNBILLMinorUnit) {
        this.PLNBILLMinorUnit = PLNBILLMinorUnit;
        return this;
    }

    @XmlTransient
    public double getPLNBILLValue() {
        return PLNBILLValue;
    }

    public Payment setPLNBILLValue(double PLNBILLValue) {
        this.PLNBILLValue = PLNBILLValue;
        return this;
    }

    @XmlTransient
    public int getAdministrationChargeMinorUnit() {
        return AdministrationChargeMinorUnit;
    }

    public Payment setAdministrationChargeMinorUnit(int administrationChargeMinorUnit) {
        AdministrationChargeMinorUnit = administrationChargeMinorUnit;
        return this;
    }

    public double getAdmin() {
        return admin;
    }

    public Payment setAdmin(double admin) {
        this.admin = admin;
        return this;
    }

    public String getMutationNumber() {
        return MutationNumber;
    }

    public Payment setMutationNumber(String mutationNumber) {
        MutationNumber = mutationNumber;
        return this;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public Payment setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
        return this;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public Payment setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
        return this;
    }

    @XmlTransient
    public String getBukopinReference() {
        return BukopinReference;
    }

    public Payment setBukopinReference(String bukopinReference) {
        BukopinReference = bukopinReference;
        return this;
    }

    @XmlTransient
    public String getBillcomponenttype() {
        return Billcomponenttype;
    }

    public Payment setBillcomponenttype(String billcomponenttype) {
        Billcomponenttype = billcomponenttype;
        return this;
    }

    @XmlTransient
    public int getBillComponentMinorUnit() {
        return BillComponentMinorUnit;
    }

    public Payment setBillComponentMinorUnit(int billComponentMinorUnit) {
        BillComponentMinorUnit = billComponentMinorUnit;
        return this;
    }

    @XmlTransient
    public double getBillComponentValueAmount() {
        return BillComponentValueAmount;
    }

    public Payment setBillComponentValueAmount(double billComponentValueAmount) {
        BillComponentValueAmount = billComponentValueAmount;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Payment setMessage(String message) {
        this.message = message;
        return this;
    }
}
