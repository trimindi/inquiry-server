package com.trimindi.pln.inquiryServer.response.prepaid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trimindi.pln.inquiryServer.utils.generator.BaseHelper;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 17/05/2017.
 */


@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Payment extends BaseHelper implements Serializable {
    private String Message;
    private boolean status;
    private String SwitcherID;
    private String MeterSerialNumber;
    private String SubscriberID;
    private String Flag;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String VendingReceiptNumber;
    private String SubscriberName;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private String BuyingOption;
    private int MinorUnitofAdminCharge;
    private double admin;
    private int MinorUnitofStampDuty;
    private double StampDuty;
    private int MinorUnitofValueAddedTax;
    private double ValueAddedTax;
    private int MinorUnitofPublicLightingTax;
    private double PublicLightingTax;
    private int MinorUnitofCustomerPayablesInstallment;
    private double CustomerPayablesInstallment;
    private int MinorUnitofPowerPurchase;
    private double PowerPurchase;
    private int MinorUnitofPurchasedKWHUnit;
    private int PurchasedKWHUnit;
    private String TokenNumber;
    private String DistributionCode;
    private int ServiceUnit;
    private int ServiceUnitPhone;
    private int MaxKWHLimit;
    private String TotalRepeat;

    public Payment(List<Rules> rules, boolean status) {
        this.status = status;
        this.SwitcherID = (String) rules.get(0).getResults();
        this.MeterSerialNumber= (String) rules.get(1).getResults();
        this.SubscriberID= (String) rules.get(2).getResults();
        this.Flag= (String) rules.get(3).getResults();
        this.PLNReferenceNumber= (String) rules.get(4).getResults();
        this.BukopinReferenceNumber= (String) rules.get(5).getResults();
        this.VendingReceiptNumber= (String) rules.get(6).getResults();
        this.SubscriberName= (String) rules.get(7).getResults();
        this.SubscriberSegmentation= (String) rules.get(8).getResults();
        this.PowerConsumingCategory= Integer.parseInt((String) rules.get(9).getResults());
        this.BuyingOption= (String) rules.get(10).getResults();
        this.MinorUnitofAdminCharge= Integer.parseInt((String) rules.get(11).getResults());
        this.admin = numberMinorUnit((String) rules.get(12).getResults(),this.MinorUnitofAdminCharge);
        this.MinorUnitofStampDuty= Integer.parseInt((String) rules.get(13).getResults());
        this.StampDuty= numberMinorUnit((String) rules.get(14).getResults(),this.MinorUnitofStampDuty);
        this.MinorUnitofValueAddedTax= Integer.parseInt((String) rules.get(15).getResults());
        this.ValueAddedTax= numberMinorUnit((String) rules.get(16).getResults(),this.MinorUnitofValueAddedTax);
        this.MinorUnitofPublicLightingTax= Integer.parseInt((String) rules.get(17).getResults());
        this.PublicLightingTax= numberMinorUnit((String) rules.get(18).getResults(),this.MinorUnitofPublicLightingTax);
        this.MinorUnitofCustomerPayablesInstallment= Integer.parseInt((String) rules.get(19).getResults());
        this.CustomerPayablesInstallment= numberMinorUnit((String) rules.get(20).getResults(),this.MinorUnitofCustomerPayablesInstallment);
        this.MinorUnitofPowerPurchase= Integer.parseInt((String) rules.get(21).getResults());
        this.PowerPurchase= numberMinorUnit((String) rules.get(22).getResults(),this.MinorUnitofPowerPurchase);
        this.MinorUnitofPurchasedKWHUnit= Integer.parseInt((String) rules.get(23).getResults());
        this.PurchasedKWHUnit= Integer.parseInt((String) rules.get(24).getResults());
        this.TokenNumber= (String) rules.get(25).getResults();
        this.DistributionCode = (String) rules.get(26).getResults();
        this.ServiceUnit = Integer.parseInt(((String) rules.get(27).getResults()).length() == 0 ? "0" : (String) rules.get(27).getResults());
        this.ServiceUnitPhone = Integer.parseInt(((String) rules.get(28).getResults()).length() == 0 ? "0" : (String) rules.get(28).getResults());
        this.MaxKWHLimit= Integer.parseInt((String) rules.get(29).getResults());
        this.TotalRepeat= (String) rules.get(30).getResults();
        this.Message = (String) rules.get(31).getResults();
    }

    public Payment() {
    }

    @Override
    public String toString() {
        return "Payment{" +
                "Message='" + Message + '\'' +
                ", status=" + status +
                ", SwitcherID='" + SwitcherID + '\'' +
                ", MeterSerialNumber='" + MeterSerialNumber + '\'' +
                ", SubscriberID='" + SubscriberID + '\'' +
                ", Flag='" + Flag + '\'' +
                ", PLNReferenceNumber='" + PLNReferenceNumber + '\'' +
                ", BukopinReferenceNumber='" + BukopinReferenceNumber + '\'' +
                ", VendingReceiptNumber='" + VendingReceiptNumber + '\'' +
                ", SubscriberName='" + SubscriberName + '\'' +
                ", SubscriberSegmentation='" + SubscriberSegmentation + '\'' +
                ", PowerConsumingCategory=" + PowerConsumingCategory +
                ", BuyingOption='" + BuyingOption + '\'' +
                ", MinorUnitofAdminCharge=" + MinorUnitofAdminCharge +
                ", admin=" + admin +
                ", MinorUnitofStampDuty=" + MinorUnitofStampDuty +
                ", StampDuty=" + StampDuty +
                ", MinorUnitofValueAddedTax=" + MinorUnitofValueAddedTax +
                ", ValueAddedTax=" + ValueAddedTax +
                ", MinorUnitofPublicLightingTax=" + MinorUnitofPublicLightingTax +
                ", PublicLightingTax=" + PublicLightingTax +
                ", MinorUnitofCustomerPayablesInstallment=" + MinorUnitofCustomerPayablesInstallment +
                ", CustomerPayablesInstallment=" + CustomerPayablesInstallment +
                ", MinorUnitofPowerPurchase=" + MinorUnitofPowerPurchase +
                ", PowerPurchase=" + PowerPurchase +
                ", MinorUnitofPurchasedKWHUnit=" + MinorUnitofPurchasedKWHUnit +
                ", PurchasedKWHUnit=" + PurchasedKWHUnit +
                ", TokenNumber='" + TokenNumber + '\'' +
                ", DistributionCode='" + DistributionCode + '\'' +
                ", ServiceUnit=" + ServiceUnit +
                ", ServiceUnitPhone=" + ServiceUnitPhone +
                ", MaxKWHLimit=" + MaxKWHLimit +
                ", TotalRepeat='" + TotalRepeat + '\'' +
                '}';
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    @XmlTransient
    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @XmlTransient
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getMeterSerialNumber() {
        return MeterSerialNumber;
    }

    public void setMeterSerialNumber(String meterSerialNumber) {
        MeterSerialNumber = meterSerialNumber;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
    }

    @XmlTransient
    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public void setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
    }

    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public void setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
    }

    @XmlTransient
    public String getVendingReceiptNumber() {
        return VendingReceiptNumber;
    }

    public void setVendingReceiptNumber(String vendingReceiptNumber) {
        VendingReceiptNumber = vendingReceiptNumber;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public void setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public void setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
    }

    @XmlTransient
    public String getBuyingOption() {
        return BuyingOption;
    }

    public void setBuyingOption(String buyingOption) {
        BuyingOption = buyingOption;
    }

    @XmlTransient
    public int getMinorUnitofAdminCharge() {
        return MinorUnitofAdminCharge;
    }

    public void setMinorUnitofAdminCharge(int minorUnitofAdminCharge) {
        MinorUnitofAdminCharge = minorUnitofAdminCharge;
    }

    public double getAdmin() {
        return admin;
    }

    public void setAdmin(double admin) {
        this.admin = admin;
    }

    @XmlTransient
    public int getMinorUnitofStampDuty() {
        return MinorUnitofStampDuty;
    }

    public void setMinorUnitofStampDuty(int minorUnitofStampDuty) {
        MinorUnitofStampDuty = minorUnitofStampDuty;
    }

    public double getStampDuty() {
        return StampDuty;
    }

    public void setStampDuty(double stampDuty) {
        StampDuty = stampDuty;
    }

    @XmlTransient
    public int getMinorUnitofValueAddedTax() {
        return MinorUnitofValueAddedTax;
    }

    public void setMinorUnitofValueAddedTax(int minorUnitofValueAddedTax) {
        MinorUnitofValueAddedTax = minorUnitofValueAddedTax;
    }

    public double getValueAddedTax() {
        return ValueAddedTax;
    }

    public void setValueAddedTax(double valueAddedTax) {
        ValueAddedTax = valueAddedTax;
    }

    @XmlTransient
    public int getMinorUnitofPublicLightingTax() {
        return MinorUnitofPublicLightingTax;
    }

    public void setMinorUnitofPublicLightingTax(int minorUnitofPublicLightingTax) {
        MinorUnitofPublicLightingTax = minorUnitofPublicLightingTax;
    }

    public double getPublicLightingTax() {
        return PublicLightingTax;
    }

    public void setPublicLightingTax(double publicLightingTax) {
        PublicLightingTax = publicLightingTax;
    }

    @XmlTransient
    public int getMinorUnitofCustomerPayablesInstallment() {
        return MinorUnitofCustomerPayablesInstallment;
    }

    public void setMinorUnitofCustomerPayablesInstallment(int minorUnitofCustomerPayablesInstallment) {
        MinorUnitofCustomerPayablesInstallment = minorUnitofCustomerPayablesInstallment;
    }

    public double getCustomerPayablesInstallment() {
        return CustomerPayablesInstallment;
    }

    public void setCustomerPayablesInstallment(double customerPayablesInstallment) {
        CustomerPayablesInstallment = customerPayablesInstallment;
    }

    @XmlTransient
    public int getMinorUnitofPowerPurchase() {
        return MinorUnitofPowerPurchase;
    }

    public void setMinorUnitofPowerPurchase(int minorUnitofPowerPurchase) {
        MinorUnitofPowerPurchase = minorUnitofPowerPurchase;
    }

    public double getPowerPurchase() {
        return PowerPurchase;
    }

    public void setPowerPurchase(double powerPurchase) {
        PowerPurchase = powerPurchase;
    }

    @XmlTransient
    public int getMinorUnitofPurchasedKWHUnit() {
        return MinorUnitofPurchasedKWHUnit;
    }

    public void setMinorUnitofPurchasedKWHUnit(int minorUnitofPurchasedKWHUnit) {
        MinorUnitofPurchasedKWHUnit = minorUnitofPurchasedKWHUnit;
    }


    public int getPurchasedKWHUnit() {
        return PurchasedKWHUnit;
    }

    public void setPurchasedKWHUnit(int purchasedKWHUnit) {
        PurchasedKWHUnit = purchasedKWHUnit;
    }

    public String getTokenNumber() {
        return TokenNumber;
    }

    public void setTokenNumber(String tokenNumber) {
        TokenNumber = tokenNumber;
    }

    @XmlTransient
    public String getDistributionCode() {
        return DistributionCode;
    }

    public void setDistributionCode(String distributionCode) {
        DistributionCode = distributionCode;
    }

    public int getServiceUnit() {
        return ServiceUnit;
    }

    public void setServiceUnit(int serviceUnit) {
        ServiceUnit = serviceUnit;
    }

    public int getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(int serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    @XmlTransient
    public int getMaxKWHLimit() {
        return MaxKWHLimit;
    }

    public void setMaxKWHLimit(int maxKWHLimit) {
        MaxKWHLimit = maxKWHLimit;
    }

    @XmlTransient
    public String getTotalRepeat() {
        return TotalRepeat;
    }

    public void setTotalRepeat(String totalRepeat) {
        TotalRepeat = totalRepeat;
    }
}
