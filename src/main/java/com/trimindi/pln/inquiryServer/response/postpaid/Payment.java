package com.trimindi.pln.inquiryServer.response.postpaid;



import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 17/05/2017.
 */
@XmlRootElement
public class Payment implements Serializable {
    private String SwitcherID;
    private String SubscriberID;
    private int BillStatus;
    private int PaymentStatus;
    private int TotalOutstandingBill;
    private String BukopinReferenceNumber;
    private String SubscriberName;
    private int SubscriberUnit;
    private int ServiceUnitPhone;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private double admin;
    private List<Rincian> rincians;
    private String message;
    private String period;
    private String meter;
    private double denda;
    private String dueDate;

    public Payment() {
    }

    public Payment(List<Rules> rules) {
        this.SwitcherID = (String) rules.get(0).getResults();
        this.SubscriberID = (String) rules.get(1).getResults();
        this.BillStatus = Integer.parseInt((String) rules.get(2).getResults());
        this.PaymentStatus = Integer.parseInt((String) rules.get(3).getResults());
        this.TotalOutstandingBill = Integer.parseInt((String) rules.get(4).getResults());
        this.BukopinReferenceNumber = (String) rules.get(5).getResults();
        this.SubscriberName = (String) rules.get(6).getResults();
        this.SubscriberUnit = Integer.parseInt((String) rules.get(7).getResults());
        this.ServiceUnitPhone = Integer.parseInt((String) rules.get(8).getResults());
        this.SubscriberSegmentation = (String) rules.get(9).getResults();
        this.PowerConsumingCategory = Integer.parseInt((String) rules.get(10).getResults());
        this.admin = Double.parseDouble((String) rules.get(11).getResults());
        this.message = (String) rules.get(12).getResults();
    }

    @Override
    public String toString() {
        return "Payment{" +
                "SwitcherID='" + SwitcherID + '\'' +
                ", SubscriberID='" + SubscriberID + '\'' +
                ", BillStatus=" + BillStatus +
                ", PaymentStatus=" + PaymentStatus +
                ", TotalOutstandingBill=" + TotalOutstandingBill +
                ", BukopinReferenceNumber='" + BukopinReferenceNumber + '\'' +
                ", SubscriberName='" + SubscriberName + '\'' +
                ", SubscriberUnit=" + SubscriberUnit +
                ", ServiceUnitPhone=" + ServiceUnitPhone +
                ", SubscriberSegmentation='" + SubscriberSegmentation + '\'' +
                ", PowerConsumingCategory=" + PowerConsumingCategory +
                ", admin=" + admin +
                ", rincians=" + rincians +
                ", message='" + message + '\'' +
                ", period='" + period + '\'' +
                ", meter='" + meter + '\'' +
                ", denda=" + denda +
                ", dueDate='" + dueDate + '\'' +
                '}';
    }

    public String getDueDate() {
        return dueDate;
    }

    public Payment setDueDate(String dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public double getDenda() {
        return denda;
    }

    public Payment setDenda(double denda) {
        this.denda = denda;
        return this;
    }

    public String getPeriod() {
        return period;
    }

    public Payment setPeriod(String period) {
        this.period = period;
        return this;
    }

    public String getMeter() {
        return meter;
    }

    public Payment setMeter(String meter) {
        this.meter = meter;
        return this;
    }

    @XmlTransient
    public String getSwitcherID() {
        return SwitcherID;
    }

    public Payment setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
        return this;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public Payment setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
        return this;
    }

    public int getBillStatus() {
        return BillStatus;
    }

    public Payment setBillStatus(int billStatus) {
        BillStatus = billStatus;
        return this;
    }

    public int getPaymentStatus() {
        return PaymentStatus;
    }

    public Payment setPaymentStatus(int paymentStatus) {
        PaymentStatus = paymentStatus;
        return this;
    }

    public int getTotalOutstandingBill() {
        return TotalOutstandingBill;
    }

    public Payment setTotalOutstandingBill(int totalOutstandingBill) {
        TotalOutstandingBill = totalOutstandingBill;
        return this;
    }

    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public Payment setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
        return this;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public Payment setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
        return this;
    }

    public int getSubscriberUnit() {
        return SubscriberUnit;
    }

    public Payment setSubscriberUnit(int subscriberUnit) {
        SubscriberUnit = subscriberUnit;
        return this;
    }

    public int getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public Payment setServiceUnitPhone(int serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
        return this;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public Payment setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
        return this;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public Payment setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
        return this;
    }

    public double getAdmin() {
        return admin;
    }

    public Payment setAdmin(double admin) {
        this.admin = admin;
        return this;
    }

    @XmlElementWrapper(name="rincians")
    @XmlElement(name = "rincian")
    public List<Rincian> getRincians() {
        return rincians;
    }

    public Payment setRincians(List<Rincian> rincians) {
        this.rincians = rincians;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Payment setMessage(String message) {
        this.message = message;
        return this;
    }
}
