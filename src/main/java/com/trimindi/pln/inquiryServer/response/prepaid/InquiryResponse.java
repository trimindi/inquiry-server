package com.trimindi.pln.inquiryServer.response.prepaid;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by PC on 6/16/2017.
 */
@XmlRootElement
public class InquiryResponse {
    private boolean status = true;
    private String ntrans;
    private double tagihan;
    private double fee;
    private double totalBayar;
    private double saldo;
    private double totalFee;
    private Inquiry data;

    public InquiryResponse() {
    }

    @Override
    public String toString() {
        return "InquiryResponse{" +
                "status=" + status +
                ", ntrans='" + ntrans + '\'' +
                ", tagihan=" + tagihan +
                ", fee=" + fee +
                ", totalBayar=" + totalBayar +
                ", saldo=" + saldo +
                ", totalFee=" + totalFee +
                ", data=" + data +
                '}';
    }

    public boolean isStatus() {
        return status;
    }

    public InquiryResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public InquiryResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }


    public double getTagihan() {
        return tagihan;
    }

    public InquiryResponse setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public InquiryResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getTotalBayar() {
        return totalBayar;
    }

    public InquiryResponse setTotalBayar(double totalBayar) {
        this.totalBayar = totalBayar;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public InquiryResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public InquiryResponse setTotalFee(double totalFee) {
        this.totalFee = totalFee;
        return this;
    }

    public Inquiry getData() {
        return data;
    }

    public InquiryResponse setData(Inquiry data) {
        this.data = data;
        return this;
    }

}
