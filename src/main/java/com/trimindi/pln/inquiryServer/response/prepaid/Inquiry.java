package com.trimindi.pln.inquiryServer.response.prepaid;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.trimindi.pln.inquiryServer.utils.generator.BaseHelper;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 17/05/2017.
 */
@XmlRootElement
@JsonIgnoreProperties(ignoreUnknown = true)
public class Inquiry extends BaseHelper implements Serializable {
    private String SwitcherID;
    private String MeterSerialNumber;
    private String SubscriberID;
    private String Flag;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String SubscriberName;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private int MinorUnitofAdminCharge;
    private double admin;
    private double tagihan;
    private String DistributionCode;
    private int ServiceUnit;
    private int ServiceUnitPhone;
    private int MaxKWHLimit;
    private int TotalRepeat;

    public Inquiry(List<Rules> rules, boolean status) {
        this.SwitcherID = (String) rules.get(0).getResults();
        this.MeterSerialNumber = (String) rules.get(1).getResults();
        this.SubscriberID  = (String) rules.get(2).getResults();
        this.Flag  = (String) rules.get(3).getResults();
        this.PLNReferenceNumber  = (String) rules.get(4).getResults();
        this.BukopinReferenceNumber  = (String) rules.get(5).getResults();
        this.SubscriberName  = (String) rules.get(6).getResults();
        this.SubscriberSegmentation  = (String) rules.get(7).getResults();
        this.PowerConsumingCategory  = Integer.parseInt((String) rules.get(8).getResults());
        this.MinorUnitofAdminCharge  = Integer.parseInt((String) rules.get(9).getResults());
        this.admin =numberMinorUnit ((String) rules.get(10).getResults(),this.MinorUnitofAdminCharge);
        this.DistributionCode = (String) rules.get(11).getResults();
        this.ServiceUnit = Integer.parseInt(((String) rules.get(12).getResults()).length() == 0 ? "0" : (String) rules.get(12).getResults());
        this.ServiceUnitPhone = Integer.parseInt(((String) rules.get(13).getResults()).length() == 0 ? "0" : (String) rules.get(13).getResults());
        this.MaxKWHLimit = Integer.parseInt((String) rules.get(14).getResults());
        this.TotalRepeat = Integer.parseInt((String) rules.get(15).getResults());
    }

    public Inquiry() {

    }

    @Override
    public String toString() {
        return "Inquiry{" +
                "SwitcherID='" + SwitcherID + '\'' +
                ", MeterSerialNumber='" + MeterSerialNumber + '\'' +
                ", SubscriberID='" + SubscriberID + '\'' +
                ", Flag='" + Flag + '\'' +
                ", PLNReferenceNumber='" + PLNReferenceNumber + '\'' +
                ", BukopinReferenceNumber='" + BukopinReferenceNumber + '\'' +
                ", SubscriberName='" + SubscriberName + '\'' +
                ", SubscriberSegmentation='" + SubscriberSegmentation + '\'' +
                ", PowerConsumingCategory=" + PowerConsumingCategory +
                ", MinorUnitofAdminCharge=" + MinorUnitofAdminCharge +
                ", admin=" + admin +
                ", tagihan=" + tagihan +
                ", DistributionCode='" + DistributionCode + '\'' +
                ", ServiceUnit=" + ServiceUnit +
                ", ServiceUnitPhone=" + ServiceUnitPhone +
                ", MaxKWHLimit=" + MaxKWHLimit +
                ", TotalRepeat=" + TotalRepeat +
                '}';
    }

    public double getTagihan() {
        return tagihan;
    }

    public Inquiry setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    @XmlTransient
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getMeterSerialNumber() {
        return MeterSerialNumber;
    }

    public void setMeterSerialNumber(String meterSerialNumber) {
        MeterSerialNumber = meterSerialNumber;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
    }

    @XmlTransient
    public String getFlag() {
        return Flag;
    }

    public void setFlag(String flag) {
        Flag = flag;
    }

    @XmlTransient
    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public void setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
    }

    @XmlTransient
    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public void setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public void setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public void setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
    }

    @XmlTransient
    public int getMinorUnitofAdminCharge() {
        return MinorUnitofAdminCharge;
    }

    public void setMinorUnitofAdminCharge(int minorUnitofAdminCharge) {
        MinorUnitofAdminCharge = minorUnitofAdminCharge;
    }

    public double getAdmin() {
        return admin;
    }

    public void setAdmin(double admin) {
        this.admin = admin;
    }

    @XmlTransient
    public String getDistributionCode() {
        return DistributionCode;
    }

    public void setDistributionCode(String distributionCode) {
        DistributionCode = distributionCode;
    }

    @XmlTransient
    public int getServiceUnit() {
        return ServiceUnit;
    }

    public void setServiceUnit(int serviceUnit) {
        ServiceUnit = serviceUnit;
    }

    @XmlTransient
    public int getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(int serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    @XmlTransient
    public int getMaxKWHLimit() {
        return MaxKWHLimit;
    }

    public void setMaxKWHLimit(int maxKWHLimit) {
        MaxKWHLimit = maxKWHLimit;
    }

    @XmlTransient
    public int getTotalRepeat() {
        return TotalRepeat;
    }

    public void setTotalRepeat(int totalRepeat) {
        TotalRepeat = totalRepeat;
    }
}
