package com.trimindi.pln.inquiryServer.response.postpaid;

import com.trimindi.pln.inquiryServer.utils.generator.BaseHelper;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */

@XmlRootElement
public class Inquiry extends BaseHelper implements Serializable{
    private String SwitcherID;
    private String SubscriberID;
    private int BillStatus;
    private int TotalOutstandingBill;
    private String BukopinTbkReferenceNumber;
    private String SubscriberName;
    private String SubscriberUnit;
    private String ServiceUnitPhone;
    private String SubscriberSegmentation;
    private int PowerConsumingCategory;
    private double admin;
    private String duedate;
    private List<Rincian> rincian;
    private String period;
    private String meter;
    private double tagihan;
    private double denda;

    public Inquiry(List<Rules> rules, boolean status) {
        this.SwitcherID = (String) rules.get(0).getResults();
        this.SubscriberID = (String) rules.get(1).getResults();
        this.BillStatus = Integer.parseInt((String) rules.get(2).getResults());
        this.TotalOutstandingBill = Integer.parseInt((String) rules.get(3).getResults());
        this.BukopinTbkReferenceNumber = (String) rules.get(4).getResults();
        this.SubscriberName = (String) rules.get(5).getResults();
        this.SubscriberUnit = (String) rules.get(6).getResults();
        this.ServiceUnitPhone = (String) rules.get(7).getResults();
        this.SubscriberSegmentation = (String) rules.get(8).getResults();
        this.PowerConsumingCategory = Integer.parseInt((String) rules.get(9).getResults());
        this.admin = Double.parseDouble((String) rules.get(10).getResults());
    }

    public Inquiry() {

    }

    @Override
    public String toString() {
        return "Inquiry{" +
                "SwitcherID='" + SwitcherID + '\'' +
                ", SubscriberID='" + SubscriberID + '\'' +
                ", BillStatus=" + BillStatus +
                ", TotalOutstandingBill=" + TotalOutstandingBill +
                ", BukopinTbkReferenceNumber='" + BukopinTbkReferenceNumber + '\'' +
                ", SubscriberName='" + SubscriberName + '\'' +
                ", SubscriberUnit='" + SubscriberUnit + '\'' +
                ", ServiceUnitPhone='" + ServiceUnitPhone + '\'' +
                ", SubscriberSegmentation='" + SubscriberSegmentation + '\'' +
                ", PowerConsumingCategory=" + PowerConsumingCategory +
                ", admin=" + admin +
                ", duedate='" + duedate + '\'' +
                ", rincian=" + rincian +
                ", period='" + period + '\'' +
                ", meter='" + meter + '\'' +
                ", tagihan=" + tagihan +
                ", denda=" + denda +
                '}';
    }

    public double getDenda() {
        return denda;
    }

    public Inquiry setDenda(double denda) {
        this.denda = denda;
        return this;
    }

    public String getDuedate() {
        return duedate;
    }

    public Inquiry setDuedate(String duedate) {
        this.duedate = duedate;
        return this;
    }

    public double getTagihan() {
        return tagihan;
    }

    public Inquiry setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public String getPeriod() {
        return period;
    }

    public Inquiry setPeriod(String period) {
        this.period = period;
        return this;
    }

    public String getMeter() {
        return meter;
    }

    public Inquiry setMeter(String meter) {
        this.meter = meter;
        return this;
    }

    @XmlElementWrapper(name="rincians")
    @XmlElement(name = "rincian")
    public List<Rincian> getRincian() {
        return rincian;
    }

    public Inquiry setRincian(List<Rincian> rincian) {
        this.rincian = rincian;
        return this;
    }

    @XmlTransient
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
    }

    public int getBillStatus() {
        return BillStatus;
    }

    public void setBillStatus(int billStatus) {
        BillStatus = billStatus;
    }

    public int getTotalOutstandingBill() {
        return TotalOutstandingBill;
    }

    public void setTotalOutstandingBill(int totalOutstandingBill) {
        TotalOutstandingBill = totalOutstandingBill;
    }

    @XmlTransient
    public String getBukopinTbkReferenceNumber() {
        return BukopinTbkReferenceNumber;
    }

    public void setBukopinTbkReferenceNumber(String bukopinTbkReferenceNumber) {
        BukopinTbkReferenceNumber = bukopinTbkReferenceNumber;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
    }

    @XmlTransient
    public String getSubscriberUnit() {
        return SubscriberUnit;
    }

    public void setSubscriberUnit(String subscriberUnit) {
        SubscriberUnit = subscriberUnit;
    }

    @XmlTransient
    public String getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(String serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    public String getSubscriberSegmentation() {
        return SubscriberSegmentation;
    }

    public void setSubscriberSegmentation(String subscriberSegmentation) {
        SubscriberSegmentation = subscriberSegmentation;
    }

    public int getPowerConsumingCategory() {
        return PowerConsumingCategory;
    }

    public void setPowerConsumingCategory(int powerConsumingCategory) {
        PowerConsumingCategory = powerConsumingCategory;
    }

    public double getAdmin() {
        return admin;
    }

    public void setAdmin(double admin) {
        this.admin = admin;
    }

}
