package com.trimindi.pln.inquiryServer.response.nontaglist;

import com.trimindi.pln.inquiryServer.utils.generator.BaseHelper;
import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;
import java.util.List;

/**
 * Created by HP on 18/05/2017.
 */
@XmlRootElement
public class Inquiry extends BaseHelper implements Serializable {
    private String SwitcherID;
    private String RegistrationNumber;
    private String AreaCode;
    private String TransactionCode;
    private String TransactionName;
    private String RegistrationDate;
    private String ExpirationDate;
    private String SubscriberID;
    private String SubscriberName;
    private String PLNReferenceNumber;
    private String BukopinReferenceNumber;
    private String ServiceUnit;
    private String ServiceUnitAddress;
    private String ServiceUnitPhone;
    private int TotalTransactionAmountMinorUnit;
    private double tagihan;
    private int PLNBILLMinorUnit;
    private double PLNBILLValue;
    private int AdministrationChargeMinorUnit;
    private double admin;

    /**
     * 62
     */

    private String Billcomponenttype;
    private int BillComponentMinorUnit;
    private double BillComponentValueAmount;

    public Inquiry(List<Rules> rules, boolean status) {
        this.SwitcherID = (String) rules.get(0).getResults();
        this.RegistrationNumber = (String) rules.get(1).getResults();
        this.AreaCode = (String) rules.get(2).getResults();
        this.TransactionCode = (String) rules.get(3).getResults();
        this.TransactionName = (String) rules.get(4).getResults();
        this.RegistrationDate = (String) rules.get(5).getResults();
        this.ExpirationDate = (String) rules.get(6).getResults();
        this.SubscriberID = (String) rules.get(7).getResults();
        this.SubscriberName = (String) rules.get(8).getResults();
        this.PLNReferenceNumber = (String) rules.get(9).getResults();
        this.BukopinReferenceNumber = (String) rules.get(10).getResults();
        this.ServiceUnit = (String) rules.get(11).getResults();
        this.ServiceUnitAddress = (String) rules.get(12).getResults();
        this.ServiceUnitPhone = (String) rules.get(13).getResults();
        this.TotalTransactionAmountMinorUnit = Integer.parseInt((String) rules.get(14).getResults());
        this.tagihan = numberMinorUnit((String) rules.get(15).getResults(),this.TotalTransactionAmountMinorUnit);
        this.PLNBILLMinorUnit = Integer.parseInt((String) rules.get(16).getResults());
        this.PLNBILLValue = numberMinorUnit((String) rules.get(17).getResults(),this.PLNBILLMinorUnit);
        this.AdministrationChargeMinorUnit = Integer.parseInt((String) rules.get(18).getResults());
        this.admin = numberMinorUnit((String) rules.get(19).getResults(),this.AdministrationChargeMinorUnit);
        this.Billcomponenttype = (String) rules.get(20).getResults();
        this.BillComponentMinorUnit = Integer.parseInt((String) rules.get(21).getResults());
        this.BillComponentValueAmount = numberMinorUnit((String) rules.get(22).getResults(),this.BillComponentMinorUnit);
    }

    public Inquiry() {
    }

    @Override
    public String toString() {
        return "Inquiry{" +
                "SwitcherID='" + SwitcherID + '\'' +
                ", RegistrationNumber='" + RegistrationNumber + '\'' +
                ", AreaCode='" + AreaCode + '\'' +
                ", TransactionCode='" + TransactionCode + '\'' +
                ", TransactionName='" + TransactionName + '\'' +
                ", RegistrationDate='" + RegistrationDate + '\'' +
                ", ExpirationDate='" + ExpirationDate + '\'' +
                ", SubscriberID='" + SubscriberID + '\'' +
                ", SubscriberName='" + SubscriberName + '\'' +
                ", PLNReferenceNumber='" + PLNReferenceNumber + '\'' +
                ", BukopinReferenceNumber='" + BukopinReferenceNumber + '\'' +
                ", ServiceUnit='" + ServiceUnit + '\'' +
                ", ServiceUnitAddress='" + ServiceUnitAddress + '\'' +
                ", ServiceUnitPhone='" + ServiceUnitPhone + '\'' +
                ", TotalTransactionAmountMinorUnit=" + TotalTransactionAmountMinorUnit +
                ", tagihan=" + tagihan +
                ", PLNBILLMinorUnit=" + PLNBILLMinorUnit +
                ", PLNBILLValue=" + PLNBILLValue +
                ", AdministrationChargeMinorUnit=" + AdministrationChargeMinorUnit +
                ", admin=" + admin +
                ", Billcomponenttype='" + Billcomponenttype + '\'' +
                ", BillComponentMinorUnit=" + BillComponentMinorUnit +
                ", BillComponentValueAmount=" + BillComponentValueAmount +
                '}';
    }

    @XmlTransient
    public String getSwitcherID() {
        return SwitcherID;
    }

    public void setSwitcherID(String switcherID) {
        SwitcherID = switcherID;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        RegistrationNumber = registrationNumber;
    }

    @XmlTransient
    public String getAreaCode() {
        return AreaCode;
    }

    public void setAreaCode(String areaCode) {
        AreaCode = areaCode;
    }

    @XmlTransient
    public String getTransactionCode() {
        return TransactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        TransactionCode = transactionCode;
    }

    public String getTransactionName() {
        return TransactionName;
    }

    public void setTransactionName(String transactionName) {
        TransactionName = transactionName;
    }

    public String getRegistrationDate() {
        return RegistrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        RegistrationDate = registrationDate;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public String getSubscriberID() {
        return SubscriberID;
    }

    public void setSubscriberID(String subscriberID) {
        SubscriberID = subscriberID;
    }

    public String getSubscriberName() {
        return SubscriberName;
    }

    public void setSubscriberName(String subscriberName) {
        SubscriberName = subscriberName;
    }

    @XmlTransient
    public String getPLNReferenceNumber() {
        return PLNReferenceNumber;
    }

    public void setPLNReferenceNumber(String PLNReferenceNumber) {
        this.PLNReferenceNumber = PLNReferenceNumber;
    }

    @XmlTransient
    public String getBukopinReferenceNumber() {
        return BukopinReferenceNumber;
    }

    public void setBukopinReferenceNumber(String bukopinReferenceNumber) {
        BukopinReferenceNumber = bukopinReferenceNumber;
    }

    @XmlTransient
    public String getServiceUnit() {
        return ServiceUnit;
    }

    public void setServiceUnit(String serviceUnit) {
        ServiceUnit = serviceUnit;
    }

    @XmlTransient
    public String getServiceUnitAddress() {
        return ServiceUnitAddress;
    }

    public void setServiceUnitAddress(String serviceUnitAddress) {
        ServiceUnitAddress = serviceUnitAddress;
    }

    @XmlTransient
    public String getServiceUnitPhone() {
        return ServiceUnitPhone;
    }

    public void setServiceUnitPhone(String serviceUnitPhone) {
        ServiceUnitPhone = serviceUnitPhone;
    }

    @XmlTransient
    public int getTotalTransactionAmountMinorUnit() {
        return TotalTransactionAmountMinorUnit;
    }

    public void setTotalTransactionAmountMinorUnit(int totalTransactionAmountMinorUnit) {
        TotalTransactionAmountMinorUnit = totalTransactionAmountMinorUnit;
    }

    public double getTagihan() {
        return tagihan;
    }

    public void setTagihan(double tagihan) {
        this.tagihan = tagihan;
    }

    @XmlTransient
    public int getPLNBILLMinorUnit() {
        return PLNBILLMinorUnit;
    }

    public void setPLNBILLMinorUnit(int PLNBILLMinorUnit) {
        this.PLNBILLMinorUnit = PLNBILLMinorUnit;
    }

    @XmlTransient
    public double getPLNBILLValue() {
        return PLNBILLValue;
    }

    public void setPLNBILLValue(double PLNBILLValue) {
        this.PLNBILLValue = PLNBILLValue;
    }

    @XmlTransient
    public int getAdministrationChargeMinorUnit() {
        return AdministrationChargeMinorUnit;
    }

    public void setAdministrationChargeMinorUnit(int administrationChargeMinorUnit) {
        AdministrationChargeMinorUnit = administrationChargeMinorUnit;
    }

    public double getAdmin() {
        return admin;
    }

    public void setAdmin(double admin) {
        this.admin = admin;
    }

    @XmlTransient
    public String getBillcomponenttype() {
        return Billcomponenttype;
    }

    public void setBillcomponenttype(String billcomponenttype) {
        Billcomponenttype = billcomponenttype;
    }

    @XmlTransient
    public int getBillComponentMinorUnit() {
        return BillComponentMinorUnit;
    }

    public void setBillComponentMinorUnit(int billComponentMinorUnit) {
        BillComponentMinorUnit = billComponentMinorUnit;
    }

    @XmlTransient
    public double getBillComponentValueAmount() {
        return BillComponentValueAmount;
    }

    public void setBillComponentValueAmount(double billComponentValueAmount) {
        BillComponentValueAmount = billComponentValueAmount;
    }
}
