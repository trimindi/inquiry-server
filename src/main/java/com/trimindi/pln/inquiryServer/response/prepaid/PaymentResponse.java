package com.trimindi.pln.inquiryServer.response.prepaid;

/**
 * Created by PC on 6/16/2017.
 */
public class PaymentResponse {
    private boolean status = true;
    private String ntrans;
    private double tagihan;
    private double fee;
    private double totalBayar;
    private double saldo;
    private double totalFee;
    private Payment data;

    public PaymentResponse() {
    }

    public boolean isStatus() {
        return status;
    }

    public PaymentResponse setStatus(boolean status) {
        this.status = status;
        return this;
    }

    public String getNtrans() {
        return ntrans;
    }

    public PaymentResponse setNtrans(String ntrans) {
        this.ntrans = ntrans;
        return this;
    }

    @Override
    public String toString() {
        return "PaymentResponse{" +
                "status=" + status +
                ", ntrans='" + ntrans + '\'' +
                ", tagihan=" + tagihan +
                ", fee=" + fee +
                ", totalBayar=" + totalBayar +
                ", saldo=" + saldo +
                ", totalFee=" + totalFee +
                ", data=" + data +
                '}';
    }

    public double getTagihan() {
        return tagihan;
    }

    public PaymentResponse setTagihan(double tagihan) {
        this.tagihan = tagihan;
        return this;
    }

    public double getFee() {
        return fee;
    }

    public PaymentResponse setFee(double fee) {
        this.fee = fee;
        return this;
    }

    public double getTotalBayar() {
        return totalBayar;
    }

    public PaymentResponse setTotalBayar(double totalBayar) {
        this.totalBayar = totalBayar;
        return this;
    }

    public double getSaldo() {
        return saldo;
    }

    public PaymentResponse setSaldo(double saldo) {
        this.saldo = saldo;
        return this;
    }

    public double getTotalFee() {
        return totalFee;
    }

    public PaymentResponse setTotalFee(double totalFee) {
        this.totalFee = totalFee;
        return this;
    }

    public Payment getData() {
        return data;
    }

    public PaymentResponse setData(Payment data) {
        this.data = data;
        return this;
    }

}
