package com.trimindi.pln.inquiryServer.response.postpaid;


import com.trimindi.pln.inquiryServer.utils.iso.models.Rules;

import java.util.List;

/**
 * Created by PC on 6/6/2017.
 */
public class Rincian {
    private String BillPeriod;
    private String DueDate;
    private String MeterRead;
    private double TotalElectricityBill;
    private String Incentive;
    private double AddedTax;
    private double PenaltyFee;
    private double PreviousMeter;
    private double CurrentMeter;
    private String PreviousMeterReading2;
    private String CurrentMeterReading2;
    private String PreviousMeterReading3;
    private String CurrentMeterReading3;

    public Rincian() {
    }

    public Rincian(List<Rules> rules) {
        this.BillPeriod = (String) rules.get(0).getResults();
        this.DueDate = (String) rules.get(1).getResults();
        this.MeterRead = (String) rules.get(2).getResults();
        this.TotalElectricityBill = Double.parseDouble((String) rules.get(3).getResults());
        this.Incentive = (String) rules.get(4).getResults();
        this.AddedTax = Double.parseDouble((String) rules.get(5).getResults());
        this.PenaltyFee = Double.parseDouble((String) rules.get(6).getResults());
        this.PreviousMeter = Double.parseDouble((String) rules.get(7).getResults());
        this.CurrentMeter = Double.parseDouble((String) rules.get(8).getResults());
        this.PreviousMeterReading2 = (String) rules.get(9).getResults();
        this.CurrentMeterReading2 = (String) rules.get(10).getResults();
        this.PreviousMeterReading3 = (String) rules.get(11).getResults();
        this.CurrentMeterReading3 = (String) rules.get(12).getResults();
    }

    @Override
    public String toString() {
        return "Rincian{" +
                "BillPeriod='" + BillPeriod + '\'' +
                ", DueDate='" + DueDate + '\'' +
                ", MeterRead='" + MeterRead + '\'' +
                ", TotalElectricityBill=" + TotalElectricityBill +
                ", Incentive='" + Incentive + '\'' +
                ", AddedTax=" + AddedTax +
                ", PenaltyFee=" + PenaltyFee +
                ", PreviousMeter=" + PreviousMeter +
                ", CurrentMeter=" + CurrentMeter +
                ", PreviousMeterReading2='" + PreviousMeterReading2 + '\'' +
                ", CurrentMeterReading2='" + CurrentMeterReading2 + '\'' +
                ", PreviousMeterReading3='" + PreviousMeterReading3 + '\'' +
                ", CurrentMeterReading3='" + CurrentMeterReading3 + '\'' +
                '}';
    }

    public String getBillPeriod() {
        return BillPeriod;
    }

    public Rincian setBillPeriod(String billPeriod) {
        BillPeriod = billPeriod;
        return this;
    }

    public String getDueDate() {
        return DueDate;
    }

    public Rincian setDueDate(String dueDate) {
        DueDate = dueDate;
        return this;
    }

    public String getMeterRead() {
        return MeterRead;
    }

    public Rincian setMeterRead(String meterRead) {
        MeterRead = meterRead;
        return this;
    }

    public double getTotalElectricityBill() {
        return TotalElectricityBill;
    }

    public Rincian setTotalElectricityBill(double totalElectricityBill) {
        TotalElectricityBill = totalElectricityBill;
        return this;
    }

    public String getIncentive() {
        return Incentive;
    }

    public Rincian setIncentive(String incentive) {
        Incentive = incentive;
        return this;
    }

    public double getAddedTax() {
        return AddedTax;
    }

    public Rincian setAddedTax(double addedTax) {
        AddedTax = addedTax;
        return this;
    }

    public double getPenaltyFee() {
        return PenaltyFee;
    }

    public Rincian setPenaltyFee(double penaltyFee) {
        PenaltyFee = penaltyFee;
        return this;
    }

    public double getPreviousMeter() {
        return PreviousMeter;
    }

    public Rincian setPreviousMeter(double previousMeter) {
        PreviousMeter = previousMeter;
        return this;
    }

    public double getCurrentMeter() {
        return CurrentMeter;
    }

    public Rincian setCurrentMeter(double currentMeter) {
        CurrentMeter = currentMeter;
        return this;
    }

    public String getPreviousMeterReading2() {
        return PreviousMeterReading2;
    }

    public Rincian setPreviousMeterReading2(String previousMeterReading2) {
        PreviousMeterReading2 = previousMeterReading2;
        return this;
    }

    public String getCurrentMeterReading2() {
        return CurrentMeterReading2;
    }

    public Rincian setCurrentMeterReading2(String currentMeterReading2) {
        CurrentMeterReading2 = currentMeterReading2;
        return this;
    }

    public String getPreviousMeterReading3() {
        return PreviousMeterReading3;
    }

    public Rincian setPreviousMeterReading3(String previousMeterReading3) {
        PreviousMeterReading3 = previousMeterReading3;
        return this;
    }

    public String getCurrentMeterReading3() {
        return CurrentMeterReading3;
    }

    public Rincian setCurrentMeterReading3(String currentMeterReading3) {
        CurrentMeterReading3 = currentMeterReading3;
        return this;
    }
}
