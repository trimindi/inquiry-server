package com.trimindi.pln.inquiryServer.repository;

import com.trimindi.pln.inquiryServer.models.PartnerCredential;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PC on 08/08/2017.
 */
@Repository
public interface PartnerCredentialRepository extends CrudRepository<PartnerCredential,String> {
    List<PartnerCredential> findAll();
}
