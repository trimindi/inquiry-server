package com.trimindi.pln.inquiryServer.repository;

import com.trimindi.pln.inquiryServer.models.PartnerDeposit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PC on 08/08/2017.
 */
@Repository
public interface PartnerDepositRepository extends CrudRepository<PartnerDeposit,String> {
    List<PartnerDeposit> findAll();
}
