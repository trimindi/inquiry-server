package com.trimindi.pln.inquiryServer.repository;

import com.trimindi.pln.inquiryServer.models.ProductFee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PC on 08/08/2017.
 */
@Repository
public interface ProductPriceRepository extends CrudRepository<ProductFee,String> {

    @Query(value = "select t from ProductFee t where t.PARTNER_ID = :partnerid and t.PRODUCT_ITEM = :denom")
    ProductFee findByPrice(@Param("partnerid") String partnerid, @Param("denom") String denom);

    List<ProductFee> findAll();
}
