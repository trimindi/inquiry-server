package com.trimindi.pln.inquiryServer.repository;

import com.trimindi.pln.inquiryServer.models.Transaksi;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by PC on 27/07/2017.
 */
@Repository
public interface TransaksiRepository extends CrudRepository<Transaksi,String> {
    @Query(value = "SELECT '*' FROM mst_transaksi t WHERE t.MSSIDN = :mssidn and t.ST = '01'",nativeQuery = true)
    Transaksi checkTransaksi(@Param("mssidn") String mssidn);

    @Query(value = "SELECT COALESCE(sum(t.fee),0) FROM mst_transaksi t WHERE t.PARTNERID = :partnerid AND t.st = '02'", nativeQuery = true)
    double findTotalFee(@Param("partnerid") String partnerid);

    List<Transaksi> findAll();

    @Query(value = "SELECT '*' FROM mst_transaksi t WHERE t.HOST_REF_NUMBER = :host_ref_number", nativeQuery = true)
    Transaksi findByHOST_REF_NUMBER(String host_ref_number);

}
