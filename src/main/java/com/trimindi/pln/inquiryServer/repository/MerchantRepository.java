package com.trimindi.pln.inquiryServer.repository;

import com.trimindi.pln.inquiryServer.models.Merchant;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by PC on 08/08/2017.
 */
@Repository
public interface MerchantRepository extends CrudRepository<Merchant,String> {
}
