package com.trimindi.pln.inquiryServer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

/**
 * Created by PC on 10/08/2017.
 */
@Configuration
public class AppProperties {
    @Value("${pln.pan.prepaid}")
    public  String PAN_PREPAID ;
    @Value("${pln.pan.postpaid}")
    public  String PAN_POSTPAID ;
    @Value("${pln.pan.nontaglist}")
    public  String PAN_NONTAGLIST ;
    @Value("${pln.partner.id}")
    public  String PARTNER_ID ;
    @Value("${pln.bank.code}")
    public  String BANK_CODE ;
    @Value("${pln.terminal.id}")
    public  String TERMINAL_ID ;
    @Value("${pln.switcher.id}")
    public  String SWITCHER_ID ;
    @Value("${pln.signon}")
    public  String SIGN_ON ;
    @Value("${pln.signoff}")
    public  String SIGN_OFF ;
    @Value("${pln.echo}")
    public  String ECHO_TEST ;

    @Value("${iso8583.connection.timeout}")
    public int isoTimout;
}
